import { put, takeLatest, select, all, call } from 'redux-saga/effects';
import request, { showMessageError } from '../../utils/request';
import { INITIAL_REQUEST_START, INITIAL_REQUEST_SUCCESS } from '../constants';
import { showLoader, hideLoader } from '../actions';
import { HarborsServices } from '../../utils/services/harbors';
import { ContainersServices } from '../../utils/services/containers';
import { ShippingCompaniesServices } from '../../utils/services/shippingCompanies';

export function* initialRequest() {
  const ContainerServices = new ContainersServices();
  const ShippingCompanieServices = new ShippingCompaniesServices();
  try {
    yield put(showLoader());
    const requestContainers = yield ContainerServices.getContainers();
    const requestShippingCompanies = yield ShippingCompanieServices.getShippingCompanies();

    yield all([
      put({
        type: INITIAL_REQUEST_SUCCESS,
        payload: {
          containers: requestContainers,
          shippingCompanies: requestShippingCompanies,
        },
      }),
      put(hideLoader()),
    ]);
  } catch (err) {
    console.log(err);
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

export default function* loginSaga() {
  yield takeLatest(INITIAL_REQUEST_START, initialRequest);
}
