import { put, takeLatest, select, all, call } from "redux-saga/effects";
import request, { getOptionsWithToken, postOptionsWithoutToken, showMessageError } from "../../utils/request";
import { GOOGLE_START } from "../constants";
import { showLoader, hideLoader, loginSuccess } from "../actions";
import Config from "react-native-config";
import Navigation from "../../navigation";
import AsyncStorage from "@react-native-async-storage/async-storage";

export function* Google({ payload: { navigation, ...payload } }) {
  //console.log(payload, "payload");
  const form = {
    email: payload.email,
    firstName: payload.firstName,
    lastName: payload.lastName,
    googelCode: payload.idToken || payload.googelCode,
    departmentId: payload.departmentId,
    phone: parseInt(payload.phone),
  };
  let url, option;
  const storage = yield select((state) => state);
  try {
    yield put(showLoader());

    url = `${Config.URL_API}/users/login-social-networks`;
    option = postOptionsWithoutToken(form);
    const requestToken = yield call(request, url, option);
    console.log(requestToken, "tokenGoogle");

    url = `${Config.URL_API}/whoAmI`;
    option = getOptionsWithToken(requestToken.token);
    const requestUser = yield call(request, url, option);

    yield AsyncStorage.setItem("auth", JSON.stringify({ ...storage.auth, dataUser: requestUser, tokenUser: requestToken.token }));
    const auth = yield AsyncStorage.getItem("auth");

    yield all([put(loginSuccess(JSON.parse(auth))), put(hideLoader())]);
  } catch (err) {
    console.log(err);
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

// export function* VerifyGoogle({ payload }) {

//   console.log(payload, "payload");
//   let url, option;
//   const storage = yield select((state) => state);
//   try {
//     yield put(showLoader());

//     url = `${Config.URL_API}/users/login-social-networks`;
//     option = postOptionsWithoutToken(form);
//     const requestToken = yield call(request, url, option);
//     console.log(requestToken, "tokenGoogle");

//     url = `${Config.URL_API}/whoAmI`;
//     option = getOptionsWithToken(requestToken.token);
//     const requestUser = yield call(request, url, option);

//     yield AsyncStorage.setItem("auth", JSON.stringify({ ...storage.auth, dataUser: requestUser, tokenUser: requestToken.token }));
//     const auth = yield AsyncStorage.getItem("auth");

//     yield all([put(loginSuccess(JSON.parse(auth))), put(hideLoader())]);
//   } catch (err) {
//     console.log(err);
//     yield put(hideLoader());
//     yield showMessageError(err);
//   }
// }

export default function* googleSaga() {
  yield takeLatest(GOOGLE_START, Google);
}
