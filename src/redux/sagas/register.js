import { put, takeLatest, select, all, call } from "redux-saga/effects";
import request, { postOptionsWithoutToken, showMessageError } from "../../utils/request";
import { showLoader, hideLoader, registerSuccess } from "../actions";
import Config from "react-native-config";
import { REGISTER_START, ROLE_ID } from "../constants";
import { Alert } from "react-native";

export function* Register({ payload: { navigation, ...payload } }) {
  const form = {
    firstName: payload.firstName.trim(),
    lastName: payload.lastName.trim(),
    email: payload.email,
    password: payload.password,
    phone: parseInt(payload.phone),
    confirmation: false,
    roleId: ROLE_ID,
    departmentId: payload.departmentId,
  };
  // console.log(form, 'register');
  try {
    yield put(showLoader());
    const url = `${Config.URL_API}/signup`;
    const option = postOptionsWithoutToken(form);
    const registerUser = yield call(request, url, option);
    console.log(registerUser, "registerUser");
    yield Alert.alert("Registro", "Se registro con exito, revise su correo");
    yield navigation.navigate("Login");
    yield put(hideLoader());
  } catch (err) {
    yield showMessageError(err);
  }
}

//watchers
export default function* loginSaga() {
  yield takeLatest(REGISTER_START, Register);
}
