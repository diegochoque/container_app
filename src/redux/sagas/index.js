import { all } from "redux-saga/effects";
import login from "./login";
import register from "./register";
import harbor from "./harbor";
import offers from "./offers";
import provider from "./provider";
import payment from "./payment";
import recover from "./recover";
import google from "./google";
import facebook from "./facebook";

export default function* rootSaga() {
  yield all([login(), register(), harbor(), offers(), provider(), payment(), recover(), google(), facebook()]);
}
