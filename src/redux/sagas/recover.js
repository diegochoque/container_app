import { put, takeLatest, select, all, call } from 'redux-saga/effects';
import request, { getOptionsWithToken, postOptionsWithoutToken, showMessageError } from '../../utils/request';
import { RECOVER_START } from '../constants';
import { showLoader, hideLoader, recoverSuccess } from '../actions';
import Config from 'react-native-config';
import { Alert } from 'react-native';
import Navigation from '../../navigation';

export function* Recover({ payload: { navigation, data } }) {
  let url, option;
  console.log(data,"se ejecuto con exito payload");
  const storage = yield select((state) => state);
  try {
    yield put(showLoader());

    url = `${Config.URL_API}/users/prereset`;
    option = postOptionsWithoutToken(data);
    const requestToken = yield call(request, url, option);
    yield Alert.alert("Se envio con éxito, por favor revise su correo electronico, para recuperar su contraseña.");
    yield navigation.navigate('Login');
    console.log("se ejecuto con exito");
    yield put(hideLoader());
  } catch (err) {
    console.log(err);
    yield Alert.alert("Error el correo electrónico introducido, no está registrado o es incorrecto.");
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

export default function* recoverSaga() {
  yield takeLatest(RECOVER_START, Recover);
}
