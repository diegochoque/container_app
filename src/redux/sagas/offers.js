import { put, takeLatest, select, all, call } from "redux-saga/effects";
import { showMessageError } from "../../utils/request";
import { OFFERS_START, SIGN_OUT_SUCCESS } from "../constants";
import { showLoader, hideLoader, offersSuccess } from "../actions";
import { PresalesServices } from "../../utils/services/presales";

// select -> useSelector
// put    -> useDispatch()

export function* Offers() {
  const storage = yield select((state) => state);
  const presalesServices = new PresalesServices();
  try {
    yield put(showLoader());
    const requestPresales = yield presalesServices.getPresales();
    // console.log(requestPresales, "req")
    yield all([put(offersSuccess(requestPresales)), put(hideLoader())]);
  } catch (err) {
    yield allll([put({ type: SIGN_OUT_SUCCESS }), put(hideLoader())]);
    //yield put(hideLoader());
    yield showMessageError(err);
  }
}

export default function* loginSaga() {
  yield takeLatest(OFFERS_START, Offers);
}
