import { put, takeLatest, select, all, call } from 'redux-saga/effects';
import request, { getOptionsWithToken, postOptionsWithoutToken, showMessageError } from '../../utils/request';
import { FACEBOOK_START } from '../constants';
import { showLoader, hideLoader, facebookSuccess } from '../actions';
import Config from 'react-native-config';
import Navigation from '../../navigation';

export function* Facebook({ payload:{ navigation, ...payload} }) {
 console.log(payload,"this is payload");
  const form = {
    email: payload.email,
    firstName: payload.firstName,
    lastName: payload.lastName,
    googelCode: payload.idToken,
    departmentId: payload.departmentId,
    phone: parseInt(payload.phone),
  };
  console.log(payload.idToken);
  const storage = yield select((state) => state);
  try {
    yield put(showLoader());

    url = `${Config.URL_API}/users/login-social-networks`;
    option = postOptionsWithoutToken(form);
    const requestToken = yield call(request, url, option);
    console.log(requestToken, 'Social media');

    yield all([put(facebookSuccess(requestToken)), put(hideLoader())]);
    yield navigation.navigate("Offers");
  } catch (err) {
    console.log(err);
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

export default function* googleSaga() {
  yield takeLatest(FACEBOOK_START, Facebook);
}
