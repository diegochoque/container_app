import { put, takeLatest, all, call, select } from "redux-saga/effects";
import request, { postOptions, showMessageError } from "../../utils/request";
import { showLoader, hideLoader, pagosNetSucces } from "../actions";
import Config from "react-native-config";
import { PROVIDER_START } from "../constants/provider";
import { HOSTNAME } from "../constants";
import moment from "moment";

export function* RegisterProvider({ payload: { resolve, reject, method, ...payload } }) {
  const storage = yield select((state) => state);
  let url, option, form;
  const {
    newState: { provider, startHarbor, endHarbor, shippingCompany, container, presale, destinationDate },
  } = payload;

  try {
    yield put(showLoader());

    form = {
      nameProvider: provider.nameProvider.trim(),
      phone: parseInt(provider.phone),
      wechat: parseInt(provider.wechat),
      email: provider.email,
    };

    url = `${Config.URL_API}/providers`;
    option = postOptions(form);
    const requestProvider = yield call(request, url, option);
    console.log(requestProvider, "requestProvider");

    form = {
      typePayment: method,
      dateOutput: moment(destinationDate, "DD-MM-YYYY HH:mm Z"),
      originHarborId: startHarbor.id,
      destinationHarborId: endHarbor.id,
      shippingCompanyId: shippingCompany.id,
      containerId: container.id,
      userId: storage.auth.dataUser.id,
      presaleId: presale.id,
      providerId: requestProvider.id,
    };

    url = `${Config.URL_API}/payments`;
    option = postOptions(form);
    const requestPayment = yield call(request, url, option);

    url = `${Config.URL_API}/pagos-net/${requestPayment.id}/${requestPayment.typePayment}?userId=${requestPayment.userId}&hostname=${HOSTNAME}`;
    const requestPagosNet = yield call(request, url);

    yield all([put(pagosNetSucces(requestPagosNet)), put(hideLoader())]);
    yield call(resolve, requestPagosNet);
  } catch (err) {
    yield call(reject, "Al parecer, no se pudo registrar el pago");
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

//watchers
export default function* providerSaga() {
  yield takeLatest(PROVIDER_START, RegisterProvider);
}
