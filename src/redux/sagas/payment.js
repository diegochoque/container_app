import { put, takeLatest, all, call, select } from "redux-saga/effects";
import request, { postOptions, showMessageError } from "../../utils/request";
import { showLoader, hideLoader, paymentSucces, pagosNetSucces, requestPaymentsUsersSuccess } from "../actions";
import Config from "react-native-config";
import { HOSTNAME, PAGOS_NET_START, PAYMENT_START, REQUEST_PAYMENTS_USERS_START } from "../constants";
import moment from "moment";
import { PaymentServices } from "../../utils/services/payments";

export function* PaymentRegister({ payload: { resolve, reject, typePayment, providerId, ...payload } }) {
  const storage = yield select((state) => state);

  const { newState } = payload;

  const form = {
    typePayment: typePayment,
    dateOutput: moment(newState.destinationDate, "YYYY-MM-DD HH:mm Z"),
    originHarborId: newState.startHarbor.id,
    destinationHarborId: newState.endHarbor.id,
    shippingCompanyId: newState.shippingCompany.id,
    containerId: newState.container.id,
    userId: storage.auth.dataUser.id,
    presaleId: newState.presale.id,
    providerId: providerId,
  };

  try {
    yield put(showLoader());
    const url = `${Config.URL_API}/payments`;
    const option = postOptions(form);
    const requestPayment = yield call(request, url, option);
    yield all([put(paymentSucces(requestPayment)), put(hideLoader())]);
    yield call(resolve, requestPayment);
  } catch (err) {
    yield call(reject, "No se pudo registrar el pago");
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

export function* PagosNetRegister({ payload: { resolve, reject, ...payload } }) {
  const { response } = payload;
  try {
    yield put(showLoader());
    const url = `${Config.URL_API}/pagos-net/${response.id}/${response.typePayment}?userId=${response.userId}&hostname=${HOSTNAME}`;
    const requestPagosNet = yield call(request, url);
    yield all([put(pagosNetSucces(requestPagosNet)), put(hideLoader())]);
    yield call(resolve, requestPagosNet);
  } catch (err) {
    yield call(reject, "No se pudo registrar el pago con banco");
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

export function* RequestPaymentsUsers() {
  const { dataUser } = yield select(({ auth }) => auth);
  const paymentServices = new PaymentServices();
  try {
    yield put(showLoader());
    yield paymentServices.setFilterWhereDefault("userId", dataUser.id);
    const requestPayments = yield paymentServices.getPayments();
    yield all([put(requestPaymentsUsersSuccess(requestPayments)), put(hideLoader())]);
  } catch (err) {
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

//watchers
export default function* providerSaga() {
  yield takeLatest(PAYMENT_START, PaymentRegister);
  yield takeLatest(PAGOS_NET_START, PagosNetRegister);
  yield takeLatest(REQUEST_PAYMENTS_USERS_START, RequestPaymentsUsers);
}
