import { put, takeLatest, select, all, call } from "redux-saga/effects";
import request, { getOptionsWithToken, patchOptions, postOptionsWithoutToken, showMessageError } from "../../utils/request";
import { EDIT_PROFILE_START, LOGIN_START } from "../constants";
import { showLoader, hideLoader, loginSuccess, editProfileSuccess } from "../actions";
import Config from "react-native-config";
import { Alert } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

export function* Login({ payload }) {
  let url, option;
  const storage = yield select((state) => state);
  try {
    yield put(showLoader());

    url = `${Config.URL_API}/users/login`;
    option = postOptionsWithoutToken(payload);
    const requestToken = yield call(request, url, option);
    console.log(requestToken.token, "requestToken");
    

    url = `${Config.URL_API}/whoAmI`;
    option = getOptionsWithToken(requestToken.token);
    const requestUser = yield call(request, url, option);
    
    yield AsyncStorage.setItem("auth", JSON.stringify({ ...storage.auth, dataUser: requestUser, tokenUser: requestToken.token }));
    const auth = yield AsyncStorage.getItem("auth");
  
    yield all([put(loginSuccess(JSON.parse(auth))), put(hideLoader())]);
  } catch (err) {
    console.log(err);
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

export function* EditProfile({ payload: { firstName, lastName, phone, departmentId, navigation } }) {
  const storage = yield select((state) => state);
  const form = {
    firstName: firstName.trim(),
    lastName: lastName.trim(),
    phone: parseInt(phone),
    departmentId: departmentId,
  };

  try {
    yield put(showLoader());

    const url = `${Config.URL_API}/users/${storage.auth.dataUser.id}`;
    const option = patchOptions(form);
    const requestEditProfile = yield call(request, url, option);
    yield Alert.alert("Edicion de usuario", "Se edito con éxito los datos del usuario");
    yield navigation.navigate("Offers");

    yield all([put(editProfileSuccess(form)), put(hideLoader())]);
  } catch (err) {
    console.log(err);
    yield put(hideLoader());
    yield showMessageError(err);
  }
}

export default function* loginSaga() {
  yield takeLatest(LOGIN_START, Login);
  yield takeLatest(EDIT_PROFILE_START, EditProfile);
}
