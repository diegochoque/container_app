import {
  PAGOS_NET_START,
  PAGOS_NET_SUCCESS,
  PAYMENT_START,
  PAYMENT_SUCCES,
  REQUEST_PAYMENTS_USERS_START,
  REQUEST_PAYMENTS_USERS_SUCCESS,
} from "../constants";

export const paymentStart = (payload) => ({
  type: PAYMENT_START,
  payload,
});
export const paymentSucces = (payload) => ({
  type: PAYMENT_SUCCES,
  payload,
});

export const pagosNetStart = (payload) => ({
  type: PAGOS_NET_START,
  payload,
});
export const pagosNetSucces = (payload) => ({
  type: PAGOS_NET_SUCCESS,
  payload,
});

export const requestPaymentsUsersStart = (payload) => ({
  type: REQUEST_PAYMENTS_USERS_START,
  payload,
});
export const requestPaymentsUsersSuccess = (payload) => ({
  type: REQUEST_PAYMENTS_USERS_SUCCESS,
  payload,
});
