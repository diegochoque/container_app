import { REGISTER_START, REGISTER_SUCCESS } from '../constants';

export const registerStart = (payload) => ({
  type: REGISTER_START,
  payload,
});

export const registerSuccess = (payload) => ({
  type: REGISTER_SUCCESS,
  payload,
});
