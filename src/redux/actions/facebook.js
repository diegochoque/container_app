import { FACEBOOK_START, FACEBOOK_SUCCESS } from "../constants/facebook";

export const facebookStart = (payload) => ({
  type: FACEBOOK_START,
  payload,
});

export const facebookSuccess = (payload) => ({
  type: FACEBOOK_SUCCESS,
  payload,
});



