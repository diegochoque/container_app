import { OFFERS_START, OFFERS_SUCCESS } from '../constants/offers';

export const offersStart = () => ({
  type: OFFERS_START,
});

export const offersSuccess = (payload) => ({
  type: OFFERS_SUCCESS,
  payload,
});
