import { RECOVER_START, RECOVER_SUCCESS } from "../constants/recover";

export const recoverStart = (payload) => ({
  type: RECOVER_START,
  payload,
});

export const recoverSuccess = (payload) => ({
  type: RECOVER_SUCCESS,
  payload,
});
