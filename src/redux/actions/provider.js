import { PROVIDER_START, PROVIDER_SUCCES } from '../constants/provider';

export const providerStart = (payload) => ({
  type: PROVIDER_START,
  payload,
});

export const providerSucces = (payload) => ({
  type: PROVIDER_SUCCES,
  payload,
});
