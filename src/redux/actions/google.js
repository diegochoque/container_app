import { GOOGLE_START, GOOGLE_SUCCESS } from "../constants/google";

export const googleStart = (payload) => ({
  type: GOOGLE_START,
  payload,
});

export const googleSuccess = (payload) => ({
  type: GOOGLE_SUCCESS,
  payload,
});



