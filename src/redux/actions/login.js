import {
  EDIT_PROFILE_START,
  EDIT_PROFILE_SUCCESS,
  LOGIN_START,
  LOGIN_SUCCESS,
  REQUEST_POSTS_START,
  REQUEST_POSTS_SUCCESS,
} from "../constants/login";

export const loginStart = (payload) => ({
  type: LOGIN_START,
  payload,
});

export const loginSuccess = (payload) => ({
  type: LOGIN_SUCCESS,
  payload,
});

export const requestPostsStart = (payload) => ({
  type: REQUEST_POSTS_START,
  payload,
});

export const requestPostsSuccess = (payload) => ({
  type: REQUEST_POSTS_SUCCESS,
  payload,
});

export const editProfileStart = (payload) => ({
  type: EDIT_PROFILE_START,
  payload,
});

export const editProfileSuccess = (payload) => ({
  type: EDIT_PROFILE_SUCCESS,
  payload,
});
