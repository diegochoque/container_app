import { PROVIDER_SUCCES } from "../constants/provider";

const initialStateProvider = {
  data: [],
  filter: {},
};

export function providerReducer(state = initialStateProvider, action) {
  switch (action.type) {
    case PROVIDER_SUCCES:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}

export default providerReducer;
