import { PAGOS_NET_SUCCESS, PAYMENT_SUCCES, REQUEST_PAYMENTS_USERS_SUCCESS } from "../constants";

const initialState = {
  data: [],
  filter: {},
  pagosNet: [],
};

export function paymentReducer(state = initialState, action) {
  switch (action.type) {
    case PAYMENT_SUCCES:
      return { ...state, data: action.payload };
    case PAGOS_NET_SUCCESS:
      return { ...state, pagosNet: action.payload };
    case REQUEST_PAYMENTS_USERS_SUCCESS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}

export default paymentReducer;
