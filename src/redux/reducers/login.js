import { REQUEST_POSTS_SUCCESS } from "../constants";

const initialStateLogin = {
  data: [],
  filter: {},
};

export function loginReducer(state = initialStateLogin, action) {
  switch (action.type) {
    case REQUEST_POSTS_SUCCESS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}

export default loginReducer;
