import { REGISTER_SUCCESS} from "../constants";

const initialStateLogin = {
  data: [],
  filter: {},
};

export function loginReducer(state = initialStateLogin, action) {
  switch (action.type) {
    case REGISTER_SUCCESS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}

export default loginReducer;
