import { combineReducers } from 'redux';
import auth from './auth';
import login from './login';
import settings from './settings';
import register from './register';
import port from './port';
import offers from './offers';
import provider from './provider';
import payment from './payment';
import recover from './recover';

const reducers = combineReducers({
  auth,
  login,
  settings,
  register,
  port,
  offers,
  provider,
  payment,
  recover,
});

export default reducers;
