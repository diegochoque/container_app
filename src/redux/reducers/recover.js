import { RECOVER_SUCCESS } from "../constants";

const initialStateRecover = {
  data: [],
  filter: {},
};

export function recoverReducer(state = initialStateRecover, action) {
  switch (action.type) {
    case RECOVER_SUCCESS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}

export default recoverReducer;
