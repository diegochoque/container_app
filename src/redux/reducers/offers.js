import { OFFERS_SUCCESS } from "../constants";

const inicialStateOffers = {
  data: [],
  departureDates:[],
  filter: {},
};

export function offersReducer(state = inicialStateOffers, action) {
  switch (action.type) {
    case OFFERS_SUCCESS:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}

export default offersReducer;
