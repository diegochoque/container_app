import { EDIT_PROFILE_SUCCESS, LOGIN_SUCCESS, SIGN_OUT_SUCCESS } from "../constants";

export const initialState = {
  tokenUser: null,
  dataUser: {},
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const { dataUser } = action.payload;
      return {
        ...state,
        ...action.payload,
        dataUser: { ...state.dataUser, ...dataUser },
      };
    }

    case EDIT_PROFILE_SUCCESS: {
      return {
        ...state,
        dataUser: { ...state.dataUser, ...action.payload },
      };
    }

    case SIGN_OUT_SUCCESS: {
      return {
        ...state,
        tokenUser: null,
        dataUser: {},
      };
    }
    default:
      return state;
  }
};
export default auth;
