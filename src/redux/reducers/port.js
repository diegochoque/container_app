import { INITIAL_REQUEST_SUCCESS } from '../constants';

const initialStateLogin = {
  data: [],
  originHarbors: [],
  departureHarbors: [],
  containers: [],
  shippingCompanies: [],
  originCountries: [],
  departureCountries: [],
  request: [],
  filter: {},
};

export function portReducer(state = initialStateLogin, action) {
  switch (action.type) {
    case INITIAL_REQUEST_SUCCESS:
      return { ...state, ...action.payload };
    default:
      return state;
  }
}

export default portReducer;
