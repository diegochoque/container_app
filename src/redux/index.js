import { createStore, applyMiddleware } from "redux";
import reduxSaga from "redux-saga";

import reducers from "./reducers";
import rootSaga from "./sagas";
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-async-storage/async-storage";

const sagaMiddleware = reduxSaga();

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["auth"],
  blacklist: [""],
};

const persistedReducer = persistReducer(persistConfig, reducers);

const reduxStorage = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

export let persistor = persistStore(reduxStorage);

export default reduxStorage;

