import React, { useEffect } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator, CardStyleInterpolators } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { StyleSheet, View } from "react-native";
import { SideMenu } from "../components";
import Theme from "../utils/theme";
import {
  Login,
  Register,
  Register2,
  RecoverPassword,
  PasswordVerification,
  Offers,
  ShoppingHistory,
  PortSelection,
  PortSelection1,
  PortSelection2,
  PortSelection3,
  Providers,
  PaymentHistory,
  RegisterGoogle,
  Profile,
} from "../screens";
import { Provider, useSelector } from "react-redux";
import reduxStorage from "../redux";
import Payment from "../screens/Payment";
import SplashScreen from "react-native-splash-screen";

export const storage = reduxStorage;

const StackNavigation = createStackNavigator();
const StackAuth = createStackNavigator();
const Drawer = createDrawerNavigator();

const Screens = () => (
  <StackAuth.Navigator>
    <StackAuth.Screen
      name="Offers"
      component={Offers}
      options={{ title: "Offers", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="ShoppingHistory"
      component={ShoppingHistory}
      options={{ title: "ShoppingHistory", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="PortSelection"
      component={PortSelection}
      options={{ title: "PortSelection", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="PortSelection1"
      component={PortSelection1}
      options={{ title: "PortSelection1", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="PortSelection2"
      component={PortSelection2}
      options={{ title: "PortSelection2", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="PortSelection3"
      component={PortSelection3}
      options={{ title: "PortSelection3", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="Providers"
      component={Providers}
      options={{ title: "Providers", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="Payment"
      component={Payment}
      options={{ title: "Payment", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="PaymentHistory"
      component={PaymentHistory}
      options={{ title: "PaymentHistory", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
    <StackAuth.Screen
      name="Profile"
      component={Profile}
      options={{ title: "Profile", headerShown: false, cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }}
    />
  </StackAuth.Navigator>
);

const Home = () => (
  <Drawer.Navigator
    initialRouteName="App"
    drawerContent={(props) => <SideMenu {...props} />}
    drawerStyle={styles.drawer}
    drawerPosition={"left"}
  >
    <Drawer.Screen name={"Screens"} component={Screens} />
  </Drawer.Navigator>
);

export const Main = () => {
  const { auth } = useSelector((store) => store);

  return (
    <NavigationContainer>
      <StackNavigation.Navigator>
        {auth.tokenUser ? (
          <>
            <StackNavigation.Screen
              name="App"
              component={Home}
              options={{
                title: "App",
                headerShown: false,
              }}
            />
          </>
        ) : (
          <>
            <StackNavigation.Screen
              name="Login"
              component={Login}
              options={{
                title: "Login",
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />

            <StackNavigation.Screen
              name="Register"
              component={Register}
              options={{
                title: "Register",
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />
            <StackNavigation.Screen
              name="Register2"
              component={Register2}
              options={{
                title: "Register2",
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />
            <StackNavigation.Screen
              name="RecoverPassword"
              component={RecoverPassword}
              options={{
                title: "RecoverPassword",
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />
            <StackNavigation.Screen
              name="PasswordVerification"
              component={PasswordVerification}
              options={{
                title: "PasswordVerification",
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />
            <StackNavigation.Screen
              name="RegisterGoogle"
              component={RegisterGoogle}
              options={{
                title: "RegisterGoogle",
                headerShown: false,
                cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
              }}
            />
          </>
        )}
      </StackNavigation.Navigator>
    </NavigationContainer>
  );
};

export default function Navigation() {
  useEffect(SplashScreen.hide, []);
  return (
    <Provider store={storage}>
      <Main />
    </Provider>
  );
}

const styles = StyleSheet.create({
  drawer: {
    width: 250,
    marginTop: 80,
    padding: 40,
    height: 600,
    alignSelf: "center",
    borderTopRightRadius: 28,
    borderBottomRightRadius: 28,
    backgroundColor: Theme.COLORS.WHITE,
  },
});
