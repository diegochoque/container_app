import React, { useMemo, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { StyleSheet, Text, View } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import ControllerTextInput from './components/ControllerTextInput';
import Wrapper from './components/Wrapper';

const initialState = {
  email: '',
  password: '',
};

const getInputsForm = ({ control }) => {
  return [
    {
      status: false,
      label: 'Ingrese su nombre',
      placeholder: 'Ingrese su nombre',
      name: 'firstName',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debes escribir su nombre' } },
    },
    {
      status: false,
      label: 'Ingrese su apellido',
      placeholder: 'Ingrese su apellido',
      name: 'lastName',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debes escribir su apellido' } },
    },
    {
      status: false,
      label: 'Nombre del pais donde vive',
      placeholder: 'Nombre del pais donde vive',
      name: 'country',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debes escribir el nombre del pais' } },
    },
    {
      status: false,
      label: 'Ingrese un número de teléfono',
      placeholder: 'Ingrese un número de teléfono',
      name: 'phone',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debes escribir un número telefonico' } },
    },
    {
      status: false,
      label: 'Ingrese su edad',
      placeholder: 'Ingrese su edad',
      name: 'age',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debes escribir su edad' } },
    },
    {
      status: true,
      label: 'Ingrese su correo electronico',
      placeholder: 'Ingrese su correo electronico',
      name: 'email',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debes escribir un correo electronico' } },
    },
    {
      status: true,
      label: 'Ingrese una contraseña',
      placeholder: 'Ingrese una contraseña',
      name: 'password',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debe escribir una contraseña' } },
    },
    {
      status: true,
      label: 'Repita la contraseña',
      placeholder: 'Repita la contraseña',
      name: 'repeatPassword',
      defaultValue: '',
      validateMessageError: 'Las contraseñas deben ser iguales',
      rules: {
        required: { value: true, message: 'Debe escribir una contraseña' },
        validate: () => control.getValues().password === control.getValues().repeatPassword,
      },
    },
  ];
};
const Index = () => {
  const [state, setState] = useState(initialState);
  const { control, handleSubmit, errors } = useForm();
  const onSubmit = (data) => console.log(data);
  const inputsForm = useMemo(() => getInputsForm({ control }), []);

  const handleRegister = () => {
    console.log('SE REGISTRARON LOS DATOS', state);
  };

  let request;

  return (
    <Wrapper>
      <Button mode="contained">{request?.token}</Button>
      {inputsForm
        .filter((input) => input.status)
        .map((input, index) => (
          <ControllerTextInput
            key={index}
            {...input}
            errors={errors}
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                {...input}
                style={styles.input}
                onBlur={onBlur}
                onChangeText={(value) => onChange(value)}
                value={value}
                error={errors[input.name]}
              />
            )}
          />
        ))}

      {/* <ControllerTextInput
        errors={errors}
        control={control}
        render={({ onChange, onBlur, value }) => (
          <TextInput
            style={styles.input}
            label="Password"
            onBlur={onBlur}
            onChangeText={(value) => onChange(value)}
            value={value}
            error={errors.password}
          />
        )}
        name="password"
        rules={{
          required: { value: true, message: 'Diego y Abraham huelen mal' },
          minLength: { value: 8, message: 'Debe tener al menos 8 caracteres' },
          maxLength: { value: 60, message: 'Máximo 60 caracteres' },
          pattern: {
            value: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#@$!%*?&+_-])[A-Za-z\d@#$!%*?&+_-]{8,}$/gm,
            message: 'Debe tener al menos un número, una mayúscula, minúscula y un caracter especial',
          },
        }}
        defaultValue=""
      /> */}

      <Button mode="contained" onPress={handleSubmit(onSubmit)}>
        Iniciar Sesión
      </Button>
    </Wrapper>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    marginVertical: 15,
  },
});

export default Index;
