import request, { getOptionsWithToken } from '../../utils/request';
import { StorageServices } from './storage';

export class CompanyHarborsServices extends StorageServices {
  constructor() {
    super();
  }
  async getPrice(state) {
    const {
      container: { id: containerId },
      endHarbor: { id: destinationHarborId },
      shippingCompany: { id: shippingCompanyId },
      startHarbor: { id: originHarborId },
    } = state;
    console.log(
      shippingCompanyId,
      'shippingCompanyId',
      originHarborId,
      'originHarborId',
      destinationHarborId,
      'destinationHarborId',
      containerId,
      'containerId'
    );
    const filter = {};
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo('');
      return await this.getFetchEndpoint(`company-harbors`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
const CompanyHarbors = new CompanyHarborsServices();
export default CompanyHarbors;
