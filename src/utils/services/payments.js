import request, { getOptionsWithToken } from "../request";
import { StorageServices } from "./storage";

export class PaymentServices extends StorageServices {
  constructor() {
    super();
  }
  async getPayments() {
    const filter = {
      order: "dateOutput DESC",
    };
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo("");
      return await this.getFetchEndpoint(`payments`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
  async getPaymentsUser() {
    const filter = {};
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo("");
      return await this.getFetchEndpoint(`payments`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
const Payments = new PaymentServices();
export default Payments;
