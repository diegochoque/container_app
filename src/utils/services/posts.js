import request, { getOptionsWithoutToken } from "../request";
import { StorageServices } from "./storage";

export class PostsServices extends StorageServices {
  constructor() {
    super();
  }

  async getPosts() {
    try {
      const url = `https://jsonplaceholder.typicode.com/posts`;
      const option = getOptionsWithoutToken();
      return await request(url, option);
    } catch (error) {
      console.log(error);
    }
  }
}

const Posts = new PostsServices();
export default Posts;
