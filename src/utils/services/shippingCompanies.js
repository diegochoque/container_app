import request, { getOptionsWithToken } from '../../utils/request';
import { StorageServices } from './storage';

export class ShippingCompaniesServices extends StorageServices {
  constructor() {
    super();
  }
  async getShippingCompanies() {
    const filter = {
      include: [
        {
          relation: 'shippingCompanyDepartureDate',
        },
      ],
    };
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo('');
      return await this.getFetchEndpoint(`shipping-companies`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getDepartureDates() {
    const filter = {
      order: 'endDate DESC',
    };
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo('');
      return await this.getFetchEndpoint(`departure-dates`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
const ShippingCompanies = new ShippingCompaniesServices();
export default ShippingCompanies;
