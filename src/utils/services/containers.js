import request, { getOptionsWithToken } from '../../utils/request';
import { StorageServices } from './storage';

export class ContainersServices extends StorageServices {
  constructor() {
    super();
  }
  async getContainers() {
    const filter = {};
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo('');
      return await this.getFetchEndpoint(`containers`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
const Containers = new ContainersServices();
export default Containers;
