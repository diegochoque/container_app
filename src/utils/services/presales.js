import { StorageServices } from "./storage";

export class PresalesServices extends StorageServices {
  constructor() {
    super();
  }

  async getPresales() {
    const filter = {
      order: "endDate DESC",
      where: {
        //and: [{ startDate: { lt: moment() } }, { endDate: { gt: moment() } }],
        discountRate: { neq: 1 },
      },
      include: [
        {
          relation: "presaleCompany",
          scope: {
            include: [
              {
                relation: "shippingCompanyDepartureDate",
              },
            ],
          },
        },
        {
          relation: "container",
        },
      ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`presales`);
    } catch (e) {
      //dispatch(signOut)
      console.log(e);
      return [];
    }
  }

  async getPresaleId(state) {
    const {
      container: { id: containerId },
      shippingCompany: { id: shippingCompanyId },
      endHarbor: { id: destinationHarborId },
      startHarbor: { id: originHarborId },
    } = state;

    const filter = {
      where: {
        originHarborId: originHarborId,
        destinationHarborId: destinationHarborId,
        shippingCompanyId: shippingCompanyId,
        containerId: containerId,
      },
    };

    console.log(filter, "getPresaleId");
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`company-harbors`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
const Presales = new PresalesServices();
export default Presales;
