import {StorageServices} from './storage';

export class DepartmentsServices extends StorageServices {
  constructor() {
    super();
  }
  async getDepartments() {
    const filter = {};
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo('');
      return await this.getFetchEndpoint(`departments`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
const Departments = new DepartmentsServices();
export default Departments;
