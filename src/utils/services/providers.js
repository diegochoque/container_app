import request, { getOptionsWithToken } from '../request';
import { StorageServices } from './storage';

export class ProvidersServices extends StorageServices {
  constructor() {
    super();
  }
  async getProviders() {
    const filter = {};
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo('');
      return await this.getFetchEndpoint(`providers`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
  async getProvidersPayments() {
    const filter = {
      include: [
        {
          relation: 'providerPayments',
        },
      ],
    };
    try {
      this.setFilterEndpoint(filter);
      this.setFormatNestedTo('');
      return await this.getFetchEndpoint(`providers`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
const Providers = new ProvidersServices();
export default Providers;
