import request, { getOptionsWithToken } from '../../utils/request';
import { StorageServices } from './storage';

export class HarborsServices extends StorageServices {
  constructor() {
    super();
  }
  async getHarbors() {
    const filter = {};
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`harbors`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getOriginHarbors() {
    const filter = {
      include: [
        {
          relation: 'companyOriginHarbor',
          scope: {
            include: [
              {
                relation: 'harborCountries',
              },
            ],
          },
        },
      ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`company-harbors`);
    } catch (e) {
      console.log(e, 'error');
      return [];
    }
  }

  async getDepartureHarbors() {
    const filter = {
      include: [
        {
          relation: 'companyDestinationHarbor',
          scope: {
            include: [
              {
                relation: 'harborCountries',
              },
            ],
          },
        },
      ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`company-harbors`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
  async getContainers() {
    const filter = {
      include: [
        {
          relation: 'companyContainer',
        },
      ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`company-harbors`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getOriginCountries() {
    const filter = {
      where: {
        harborType: false,
      },
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`countries`);
    } catch (e) {
      console.log(e, 'error');
      return [];
    }
  }

  async getDepartureCountries() {
    const filter = {
      where: {
        harborType: true,
      },
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`countries`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}

const Harbors = new HarborsServices();
export default Harbors;
