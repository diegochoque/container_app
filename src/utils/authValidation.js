export function validateForm(form) {
  let valid = true;
  for (let key in form) {
    if (form[key].required && validateRequired(form[key].value)) {
      valid = false;
      break;
    }
  }
  const password = validatePassword(form.password.value, form.rePassword ? form.rePassword.value : form.password.value);
  if (!validateEmail(form.email.value) || password) {
    valid = false;
  }
  return valid;
}
export function validateFormEmail(form) {
  let valid = true;
  for (let key in form) {
    if (form[key].required && validateRequired(form[key].value)) {
      valid = false;
      break;
    }
  }
  if (!validateEmail(form.value)) {
    valid = false;
  }
  return valid;
}

export function validateEditForm(form) {
  let valid = true;
  for (let key in form) {
    if (form[key].required && validateRequired(form[key].value)) {
      valid = false;
      break;
    }
  }
  return valid;
}

export function validateRequired(item) {
  return item === "";
}

export function validateEmail(email) {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email);
}

export function validatePassword(password, rePassword) {
  return password.length < 8 || password !== rePassword;
}

export function parseForm(form, type) {
  let parsedForm = {};
  switch (type) {
    case "login":
      parsedForm = {
        ...form,
        email: form.email.value,
        password: form.password.value,
      };
      break;
    case "editUser":
      parsedForm = {
        firstName: form.firstName.value,
        lastName: form.lastName.value,
        email: form.email.value,
        identityCard: form.identityCard.value,
      };
      if (form.gender.value !== "") {
        parsedForm.gender = form.gender.value;
      }
      if (form.codeReference.value !== "") {
        parsedForm.codeReference = form.codeReference.value;
      }
      if (form.state.value !== "") {
        parsedForm.userStateId = form.state.value;
      }
      if (form.institution.value !== "") {
        parsedForm.instituteId = form.institution.value;
      }
      if (form.affiliationDate.value !== "") {
        parsedForm.affiliationDate = form.affiliationDate.value;
      }
      break;
    case "signup":
      parsedForm = {
        firstName: form.firstName.value,
        lastName: form.lastName.value,
        email: form.email.value,
        phone: parseInt(form.phone.value),
        password: form.password.value,
        roleId: "0ac2b8aa-bef6-446c-8203-5ac1021fb61b",
        confirmation: false,
        departmentId: "aa98ac64-9123-4301-808d-43f2839188ae",
      };
      break;
    case "resetPassword":
      {
        parsedForm = {
          email: form.value,
        };
      }
      break;
  }

  return parsedForm;
}
