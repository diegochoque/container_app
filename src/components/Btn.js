import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-paper';
export default function Btn({ title, color, loader, width = '100%', onPress, icon }) {
  return (
    <Button
      style={[styles.submitButton, { backgroundColor: color, width: width }]}
      labelStyle={{ color: 'white' }}
      loading={loader}
      mode="contained"
      onPress={onPress}
      icon={icon}
    >
      {title}
    </Button>
  );
}

const styles = StyleSheet.create({
  submitButton: {
    height: 50,
    borderRadius: 25,
    justifyContent: 'center',
  },
});
