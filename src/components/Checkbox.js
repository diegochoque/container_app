import React from "react";
import { Checkbox as GalioCheckbox } from "galio-framework";
import Theme from "../utils/theme";

export default function Checkbox({ color, ...props }) {
  return (
    <GalioCheckbox
      style={{ margin: 10 }}
      color={Theme.COLORS[color.toUpperCase()]}
      {...props}
    />
  );
}
