import React from "react";
import { Text, View } from "react-native";
import { Icon } from "galio-framework";
import theme from "../utils/theme";
import moment from "moment";
import "moment/locale/es";

const HistoryItem = ({ typePayment, price, status, dateOutput, code }) => {
  return (
    <View style={{ flex: 1, paddingHorizontal: 10 }}>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <Icon name={"check-circle"} family={"MaterialIcons"} color={"#FFB900"} size={50} style={{ margin: 10 }} />
        <View style={{ justifyContent: "center" }}>
          <Text style={{ fontWeight: "bold", fontSize: 18, color: theme.COLORS.TITLE }}>{typePayment}</Text>
          <Text style={{ textAlign: "center", color: theme.COLORS.BLACK }}>{status[0]}</Text>
        </View>
        <View style={{ alignItems: "flex-end", justifyContent: "center", flex: 1 }}>
          <Text style={{ textAlign: "center", fontSize: 18, fontWeight: "bold", color: theme.COLORS.TITLE }}>${price}</Text>
          <Text style={{ textAlign: "center" }}>{moment(dateOutput).format('LLL')}</Text>
        </View>
      </View>
    </View>
  );
};

export default HistoryItem;
