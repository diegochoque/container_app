import * as React from 'react';
import { Alert } from 'react-native';

const Message = ({ navigation }) => {
  return (
    <View style={{ flex: 1, justifyContent: 'center' }}>
      {Alert.alert('Registro', 'Se registro con exito, se envio un mensaje de confirmacion a su correo', [
        {
          text: 'Aceptar',
          onPress: () => navigation.navigate('Login'),
        },
      ])}
    </View>
  );
};

export default Message;
