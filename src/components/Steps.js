import React from 'react';
import { StyleSheet, View, TouchableHighlight, Text } from "react-native";
import { Button } from "galio-framework";
import Theme from "../utils/theme";
import { create } from 'lodash';

export default function Steps({
    title,
    color,
    position = "center",
    marginT,
}){
    return (
        <View style={styles.rectangle}>
            <Button style={styles.btn}><Text style={styles.number}>1</Text></Button>
            <View style={styles.line}/>
            <Button style={styles.btn1}><Text style={styles.number}>2</Text></Button>
            <View style={styles.line}/>
            <Button style={styles.btn1}><Text style={styles.number}>3</Text></Button>
            <View style={styles.line}/>
            <Button style={styles.btn1}><Text style={styles.number}>4</Text></Button>
        </View>
    );
};

const styles = StyleSheet.create({
    rectangle:{
        flexDirection:'row',
        marginTop:-24,
        alignItems:'center',
    },
    btn:{
        backgroundColor:'#1FCC79',
        width:36,
        height:36,
        borderRadius:40,
    },
    btn1:{
        backgroundColor:'#D6D6D6',
        width:36,
        height:36,
        borderRadius:40,
    },
    line:{
        backgroundColor:'#1FCC79',
        width:'14%',
        height:4,
        margin:1,
        borderRadius:4
    },
    number:{
        color:'#3E5481',
        fontWeight:'bold',
        fontSize:20
    }
})