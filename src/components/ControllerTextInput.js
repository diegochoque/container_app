import React from 'react';
import { Controller } from 'react-hook-form';
import { Text } from 'react-native';
import theme from '../utils/theme';

export const ControllerTextInput = (props) => {
  return (
    <>
      <Controller {...props} />
      {props.errors[props.name] && (
        <Text style={{color: theme.COLORS.RED}}>
          {
            props.errors[props.name].type === 'validate' && !!props.validateMessageError
              ? props.validateMessageError 
              : props.errors[props.name].message
          }
        </Text>
      )}
    </>
  );
};
export default ControllerTextInput;
