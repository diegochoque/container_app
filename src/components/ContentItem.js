import React from "react";
import { View, Text, StyleSheet, Image } from "react-native";
import { Button as GalioButton, Icon } from "galio-framework";
import Theme from "../utils/theme";

export default function ContentItem({
  children,
  image,
  iconInformation,
  title,
  priceCoin,
  onPress,
  small,
  color,
  increment,
  value
}) {
  return (
    <GalioButton
      round
      shadowless
      color={color ? color : Theme.COLORS.LIGHTGREY}
      style={[
        styles.container,
        small && {
          height: 45,
          width: 280,
        },
      ]}
      onPress={onPress}
    >
      {image ? (
        <Image
          source={{
            uri: image
          }}
          style={[
            styles.image,
            small && {
              width: 30,
              height: 30,
            },
          ]}
        />
      ) : (
        iconInformation && (
          <View style={styles.image}>
            <Icon
              name={iconInformation.name}
              family={iconInformation.family}
              color={iconInformation.color}
              size={iconInformation.size}
            />
          </View>
        )
      )}
      <View
        style={
          small
            ? {
                position: "absolute",
                zIndex: -1,
                width: "100%",
                alignItems: "center",
              }
            : styles.information
        }
      >
        <Text
          style={[styles.title, small && { fontWeight: "bold", fontSize: 18 }]}
        >
          {title}
        </Text>
        {priceCoin && <Text style={styles.subTitle}>{priceCoin}
           {increment ? 
            <Text style={(increment < 0) ? styles.incrementNegative : styles.incrementPositive}>{` ${increment}%`}</Text> : null}
           </Text>}
      </View>
      {children && (
        <View style={[styles.image, { width: 80 }]}>{children}</View>
      )}
    </GalioButton>
  );
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    height: 60,
    margin: 10,
  },
  small: {
    height: 45,
    width: 200,
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: 40,
    height: 40,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  title: {
    color: Theme.COLORS.BLACK,
  },
  subTitle: {
    color: Theme.COLORS.GREY,
  },
  information: {
    width: "55%",
  },
  incrementPositive:{
    color: Theme.COLORS.SUCCESS
  },
  incrementNegative:{
    color: Theme.COLORS.ERROR
  },
});
