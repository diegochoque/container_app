import React from "react";
import { StyleSheet, View, Text, Image, Alert } from "react-native";
import { Icon } from "galio-framework";
import Theme from "../utils/theme";
import Button from "./Button";
import { useDispatch, useSelector } from "react-redux";
import { ScrollView } from "react-native-gesture-handler";
import { SIGN_OUT_SUCCESS } from "../redux/constants/login";

const items = [
  {
    iconInfo: { name: "local-offer", family: "Materialicons" },
    name: "Ofertas",
    navigateTo: "Home",
  },
  {
    iconInfo: { name: "shopping-cart", family: "FontAwesome" },
    name: "Realizar Pedido",
    navigateTo: "PortSelection",
  },
  {
    iconInfo: { name: "clipboard", family: "Entypo" },
    name: "Historial",
    navigateTo: "PaymentHistory",
  },
  {
    iconInfo: { name: "person", family: "Materialicons" },
    name: "Mi cuenta",
    navigateTo: "Profile",
  },
  {
    iconInfo: { name: "arrow-back", family: "Ionicons" },
    name: "Cerrar Sesión",
  },
];

export default function SideMenu({ navigation }) {
  const { auth } = useSelector((store) => store);
  const { firstName, lastName, email } = auth.dataUser;
  const dispatch = useDispatch();

  const signOut = () => {
    dispatch({ type: SIGN_OUT_SUCCESS });
  };

  return (
    <View style={{ justifyContent: "flex-start" }}>
      <View style={styles.user}>
        <Image style={styles.image} source={require("../../assets/images/perfil.jpg")} />
        <View style={styles.userText}>
          <Text style={styles.name}>{`${firstName} ${lastName}`}</Text>
          <Text numberOfLines={1}>{email}</Text>
        </View>
        <View style={styles.line}></View>
      </View>

      <ScrollView>
        <View style={styles.itemsContainer}>
          {items.map(({ name, iconInfo, navigateTo, options }) => (
            <Button
              key={name}
              color={"white"}
              shadowless
              highlight
              style={styles.menuItem}
              onPress={() => {
                navigateTo === "Home" ? navigation.closeDrawer() : navigateTo ? navigation.navigate(navigateTo, { options }) : signOut();
              }}
            >
              <Icon
                name={iconInfo.name}
                family={iconInfo.family}
                color={Theme.COLORS.GREY}
                size={30}
                style={{ marginLeft: 0, marginRight: 10 }}
              />
              <Text>{name}</Text>
            </Button>
          ))}
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  userText: {
    flexDirection: "column",
    marginStart: 10,
    marginTop: 15,
  },
  user: {
    display: "flex",
    height: 150,
    flexDirection: "column",
    alignItems: "center",
  },
  image: {
    height: 70,
    width: 70,
    borderRadius: 50,
  },
  name: {
    fontWeight: "bold",
    textAlign: "center",
  },
  itemsContainer: { width: "100%", height: "70%", alignItems: "center" },
  menuItem: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    width: 170,
    justifyContent: "flex-start",
  },
  line: {
    backgroundColor: "#E3E5E5",
    width: "100%",
    height: 2,
    marginTop: 13,
  },
});
