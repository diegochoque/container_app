import React from "react";
import { StyleSheet, View, Text, ScrollView, Image } from "react-native";
import { Block } from "galio-framework";
import Theme from "../utils/theme";
//import IconButton from "./IconButton";

function Ticket() {
  return (
    <View style={styles.ticketLine}>
      <View style={styles.ticketU} />
      <Text style={{ color: Theme.COLORS.GREY }}>
        - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
      </Text>
      <View style={styles.ticketU} />
    </View>
  );
}

export default function ContentCard({
  children,
  scrollable,
  color,
  ticket,
  bubble,
  bubbleSize,
  iconInfo,
}) {
  const bubbleStyles = [
    {
      width: bubbleSize,
      height: bubbleSize,
      borderColor: color,
      backgroundColor: color,
    },
    styles.bubble,
  ];
  return scrollable ? (
    <>
      {bubble && !iconInfo && <Image source={bubble} style={bubbleStyles} />}
      {iconInfo && (
        <IconButton
          name={iconInfo.name}
          family={iconInfo.family}
          size={70}
          iconColor={iconInfo.color}
          style={bubbleStyles}
        />
      )}
      <Block
        flex
        style={[
          styles.contentCard,
          color && { backgroundColor: color },
          bubble && {
            marginTop: 80,
            paddingTop: bubbleSize / 2,
            paddingBottom: 20,
          },
        ]}
      >
        <ScrollView contentContainerStyle={styles.center}>
          {ticket && Ticket()}
          {children}
        </ScrollView>
      </Block>
    </>
  ) : (
    <>
      {bubble && !iconInfo && <Image source={bubble} style={bubbleStyles} />}
      {iconInfo && (
        <IconButton
          name={iconInfo.name}
          family={iconInfo.family}
          size={60}
          iconColor={iconInfo.color}
          style={bubbleStyles}
        />
      )}
      <Block
        flex
        style={[
          styles.contentCard,
          styles.center,
          {
            paddingTop: 20,
            marginTop: 10,
          },
          color && { backgroundColor: color },
          ticket && styles.ticket,
          bubble && {
            marginTop: 80,
            paddingTop: bubbleSize / 2,
            paddingBottom: 20,
          },
          ,
        ]}
      >
        {ticket && Ticket()}
        {children}
      </Block>
    </>
  );
}

const styles = StyleSheet.create({
  contentCard: {
    width: "100%",
  },
  center: {
    display: "flex",
    alignItems: "center",
  },
  ticket: {
    width: "90%",
    height: "50%",
    paddingBottom: 20,
    justifyContent: "space-evenly",
  },
  ticketLine: {
    display: "flex",
    flexDirection: "row",
    zIndex: -1,
    position: "absolute",
    paddingTop: 85,
    width: "108%",
    justifyContent: "space-between",
    alignItems: "center",
  },
  ticketU: {
    backgroundColor: Theme.COLORS.LIGHTGREY,
    height: 30,
    width: 30,
    borderRadius: 50,
  },
  bubble: {
    borderRadius: 100,
    position: "absolute",
    zIndex: 1,
    marginTop: 100,
    borderWidth: 5,
  },
});
