import React from "react";
import { View, Text, StyleSheet, TouchableHighlight } from "react-native";
import { Icon } from "galio-framework";

export default function IconButton({
  name,
  family,
  size,
  color,
  iconColor,
  style,
  text,
  onPress,
  marginR,
  marginT,
}) {
  return (
    <View style={[styles.container, style]}>
      <TouchableHighlight>
        <Icon
          name={name}
          family={family}
          color={iconColor}
          size={size}
          onPress={onPress}
          style={{ margin: 0, marginRight: marginR, marginTop: marginT}}
        />
      </TouchableHighlight>
      {text && <Text style={{ color: iconColor, fontSize: 12 }}>{text}</Text>}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "column",
  },
});
