import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import theme from "../utils/theme";

export default function SubTitle({ subTitle, marginT }) {
  return (
    <View style={{ alignItems: "center", marginTop: marginT }}>
      <Text style={styles.subTitle}>{subTitle}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  subTitle: {
    fontSize: 15,
    color: theme.COLORS.SECONDARY,
  },
});
