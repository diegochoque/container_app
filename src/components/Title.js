import React from 'react';
import { StyleSheet, View, Text, Image } from 'react-native';
import theme from '../utils/theme';

export default function Title({ title, color }) {
  return (
    <View style={{ alignItems: 'center' }}>
      <Text style={[styles.title, {color: color}]}>{title}</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: 23,
    fontWeight: 'bold',
  },
});
