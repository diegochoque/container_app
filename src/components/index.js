import ScreenContainer from "./ScreenContainer";
import Button from "./Button";
import Header from "./Header";
import ContentCard from "./ContentCard";
import IconButton from "./IconButton";
import ContentItem from "./ContentItem";

// import Slider from "./Slider";
import Select from "./Select";
import SideMenu from "./SideMenu";
import Checkbox from "./Checkbox";
import Input from "./Input";
import Title from "./Title";
import SubTitle from "./Subtitle";
import InputSquare from "./InputSquare";
import Steps from "./Steps";
import InputPaper from "./InputPaper";

/* import DepositModal from "./DepositModal";
import CodeScanner from "./CodeScanner";
import NewsItem from "./NewsItem";
import ProductItem from "./ProductItem";
import ClasesItem from "./ClasesItem";
import ProfileComp from './ProfileComp';
import MarketCard from './MarketCard'; */

export {
  ScreenContainer,
  Button,
  Header,
  ContentCard,
  IconButton,
  ContentItem,
  /* Numpad,
  Slider, */
  Select,
  SideMenu,
  Checkbox,
  Input,
  Title,
  SubTitle,
  InputSquare,
  Steps,
  InputPaper,
  /* DepositModal,
  CodeScanner,
  NewsItem,
  ProductItem,
  ClasesItem,
  ProfileComp,
  MarketCard, */
};
