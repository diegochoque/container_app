import React from "react";
import { StyleSheet, View } from "react-native";
import Theme from "../utils/theme";

export default function ScreenContainer({ children, color, style, position = "center" }) {
  const colorStyle = color && Theme.COLORS[color.toUpperCase()];
  const containerStyles = [styles.container, color && { backgroundColor: colorStyle }, { justifyContent: position }];
  const subContainerStyles = [styles.subContainer, color && { backgroundColor: colorStyle }, { ...style }];
  return (
    <View style={containerStyles}>
      <View style={subContainerStyles}>{children}</View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
  subContainer: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    height: "80%",
  },
});
