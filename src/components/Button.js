import React from "react";
import { StyleSheet, View, TouchableHighlight } from "react-native";
import PropTypes from "prop-types";
import { Button as GalioButton } from "galio-framework";

import Theme from "../utils/theme";

class Button extends React.Component {
  render() {
    const {
      normal,
      small,
      shadowless,
      round,
      children,
      color,
      style,
      textStyle,
      disabled,
      highlight,
      ...props
    } = this.props;

    const colorStyle = color && Theme.COLORS[color.toUpperCase()];

    const buttonStyles = [
      normal && styles.normalButton,
      small && styles.smallButton,
      color && { backgroundColor: colorStyle },
      !shadowless && styles.shadow,
      styles.button,
      disabled && styles.disabled,
      { ...style },
    ];

    return !highlight ? (
      <GalioButton
        style={buttonStyles}
        shadowless
        round
        disabled={disabled}
        /* onlyIcon icon="facebook" 
        iconFamily="antdesign" 
        iconSize={30} 
        iconColor="#fff" */
        textStyle={[{ fontSize: 15, fontWeight: "700" }, disabled && { color: Theme.COLORS.DARKGREY }, textStyle]}
        {...props}
      >
        {children}
      </GalioButton>
    ) : (
      <TouchableHighlight {...props} underlayColor={colorStyle}>
        <View style={buttonStyles} textStyle={[{ fontSize: 12, fontWeight: "700" }, disabled && { color: Theme.COLORS.DARKGREY }, textStyle]}>
          {children}
        </View>
      </TouchableHighlight>
    );
  }
}

Button.propTypes = {
  normal: PropTypes.bool,
  small: PropTypes.bool,
  shadowless: PropTypes.bool,
  round: PropTypes.bool,
  color: PropTypes.oneOfType([PropTypes.string, PropTypes.oneOf(["default", "primary", "secondary", "info", "error", "success", "warning", "white"])]),
};

const styles = StyleSheet.create({
  normalButton: {
    width: 327,
    height: 56,
  },
  smallButton: {
    width: 200,
    height: 50,
  },
  button: {
    margin: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  shadow: {
    shadowColor: "black",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 4,
    shadowOpacity: 0.1,
    elevation: 2,
  },
  disabled: {
    backgroundColor: Theme.COLORS.GREY,
  },
});

export default Button;
