import React from 'react';
import { StyleSheet, Text, View, Dimensions, ScrollView } from 'react-native';

export const Wrapper = ({ children }) => {
  return (
    <View style={{ flex: 1 }}>
      <ScrollView>
        <View style={styles.container}>{children}</View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
  },
});

export default Wrapper;
