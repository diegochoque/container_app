import React from "react";
import { StyleSheet } from "react-native";
import PropTypes from "prop-types";
import { Input } from "galio-framework";
import Theme from "../utils/theme";

class ArInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      height: 44,
    };
  }
  render() {
    const { shadowless, success, error, round, onChangeText } = this.props;

    const inputStyles = [styles.input, !shadowless && styles.shadow, success && styles.success, error && styles.error, { height: this.state.height }, { ...this.props.style }];

    return (
      <Input
        placeholder="Escribe aquí..."
        placeholderTextColor={Theme.COLORS.GREYPRIMARY}
        style={inputStyles}
        color={Theme.COLORS.DARKGREY}
        rounded={round}
        {...this.props}
        onChangeText={onChangeText}
        onContentSizeChange={(event) => {
          if (event) {
            const height = event.nativeEvent.contentSize.height;
            this.setState(() => ({
              height: height + 20,
            }));
          }
        }}
      />
    );
  }
}

ArInput.defaultProps = {
  shadowless: false,
  success: false,
  error: false,
};

ArInput.propTypes = {
  shadowless: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.bool,
};

const styles = StyleSheet.create({
  input: {
    borderColor: Theme.COLORS.GREYBORDER,
    backgroundColor: "#FFFFFF",
    width: 350,
    borderRadius:40
  },
  success: {
    borderColor: Theme.COLORS.SUCCESS,
  },
  error: {
    borderColor: Theme.COLORS.ERROR,
  },
  shadow: {
    shadowColor: Theme.COLORS.BLACK,
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 2,
    shadowOpacity: 0.05,
    elevation: 2,
  },
});

export default ArInput;
