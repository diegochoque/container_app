import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import { Picker } from '@react-native-community/picker';
import Theme from "../utils/theme";

export default function Select({ items, style, widthSelect, onValueChange, ...props }) {
  const [selectedValue, setSelectedValue] = useState({});
  return (
    <View style={[styles.container, {width: widthSelect}, style]}>
      <Picker
        selectedValue={selectedValue}
        style={{ height: 40, width: 170 }}
        onValueChange={(itemValue) => {
          setSelectedValue(itemValue);
          onValueChange(itemValue);
        }}
        itemStyle={{
          color: Theme.COLORS.GREY,
        }}
        {...props}
      >
        {items.map(({ value, label }, index) => (
          <Picker.Item
            key={label + index}
            label={label}
            value={value}
            style={{
              color: Theme.COLORS.GREY,
            }}
          />
        ))}
      </Picker>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    backgroundColor: Theme.COLORS.PRIMARY,
    borderRadius: 8,
    width: 300,
    margin: 10,
    color: Theme.COLORS.GREY,
  },
});
