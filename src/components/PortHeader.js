import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import IconButton from "./IconButton";

function emptyIcon() {
  return <View style={{ width: 40, height: 40 }} />;
}
export default function PortHeader({ title, subtitle, back, color, backgroundColor, navigation, noMenu, logo }) {
  return (
    <View style={styles.main}>
      <View style={styles.header}>
        <IconButton name="keyboard-arrow-left" family="MaterialIcons" size={40} iconColor={color} color={backgroundColor} onPress={() => navigation.navigate("Offers")} />
        <View style={logo ? { flexDirection: "row", alignItems: "center" } : { alignItems: "center" }}>
          {logo && <Image source={logo} style={styles.image} />}
          <Text style={[styles.title, { color }]}>{title}</Text>

          {subtitle && <Text style={[styles.subtitle, { color }]}>{subtitle}</Text>}
        </View>
      </View>
      <View style={styles.line}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    display: "flex",
    alignItems: "center",
    width: "100%",
    height: "14%",
    paddingTop: 2,
  },
  header: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    width: "95%",
    paddingTop: 20,
    paddingBottom: 10,
  },
  title: {
    fontSize: 22,
    fontWeight: "bold",
  },
  subtitle: {
    fontSize: 10,
  },
  image: {
    alignItems: "center",
    justifyContent: "center",
    width: 30,
    height: 30,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  line: {
    backgroundColor: "#F4F5F7",
    width: "100%",
    height: 8,
    marginTop: 22,
  },
});
