import React, { useMemo } from 'react';
import { StyleSheet, Text } from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import ControllerTextInput from '../components/ControllerTextInput';
import Wrapper from '../components/Wrapper';
import Theme from '../utils/theme';
import Icon from 'react-native-vector-icons/Feather';
import { SubTitle } from '../components';

const getInputsForm = () => {
  return [
    {
      label: 'Nombre del proveedor',
      icon: 'user',
      name: 'nameProvider',
      defaultValue: '',
      rules: { required: { value: true, message: 'Debe escribir su nombre' } },
    },
    {
      label: 'Telefono del proveedor',
      name: 'phone',
      icon: 'phone',
      defaultValue: '',
      rules: {
        minLength: { value: 8, message: 'Debe tener al menos 8 caracteres' },
        required: { value: true, message: 'Debe escribir el telefono del proveedor' },
        pattern: {
          value: /^[0-9]{0,}$/,
          message: 'El telefono debe ser un numero',
        },
      },
    },
    {
      label: 'Wechat',
      name: 'wechat',
      icon: 'phone',
      defaultValue: '',
      rules: {
        minLength: { value: 8, message: 'Debe tener al menos 8 caracteres' },
        required: { value: true, message: 'Debes escribir un número de wechat' },
        pattern: {
          value: /^[0-9]{0,}$/,
          message: 'El wechat debe ser un numero',
        },
      },
    },
    {
      label: 'Correo electronico',
      name: 'email',
      icon: 'mail',
      defaultValue: '',
      rules: {
        required: { value: true, message: 'Debes escribir un correo electronico' },
        pattern: {
          value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
          message: 'Debe tener un formato de email valido',
        },
      },
    },
  ];
};

export default function Provider({ control, errors }) {
  const inputsForm = useMemo(() => getInputsForm({ control }), []);

  const inputTheme = {
    placeholder: Theme.COLORS.SECONDARY,
    text: 'black',
    primary: Theme.COLORS.PRIMARY,
  };
  return (
    <Wrapper>
      <SubTitle subTitle={'Elija los datos de su proveedor de mercaderia'} />
      {inputsForm.map((input, index) => (
        <ControllerTextInput
          key={index}
          {...input}
          errors={errors}
          control={control}
          render={({ onChange, onBlur, value }) => (
            <TextInput
              {...input}
              left={
                <TextInput.Icon
                  name={() => <Icon name={input.icon} color={Theme.COLORS.SECONDARY} />}
                  style={styles.icon}
                />
              }
              style={styles.input}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors[input.name]}
              mode={'outlined'}
              theme={{ colors: inputTheme }}
            />
          )}
        />
      ))}
    </Wrapper>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignSelf: 'center',
  },
  btnNext: {
    marginTop: '10%',
  },
  all: {
    marginEnd: '42%',
    marginTop: '2%',
    color: '#3E5481',
    fontSize: 15,
    fontWeight: 'bold',
  },
  input: {
    marginTop: 10,
    height: 50,
  },
  icon: {
    color: 'red',
    marginTop: 15,
  },
});
