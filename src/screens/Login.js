import React, { useEffect, useMemo } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { useForm } from "react-hook-form";
import { Title } from "../components";
import { useDispatch, useSelector } from "react-redux";
import Theme from "../utils/theme";
import SubTitle from "../components/Subtitle";
import { googleStart, loginStart } from "../redux/actions";
import { TextInput } from "react-native-paper";
import ControllerTextInput from "../components/ControllerTextInput";
import Icon from "react-native-vector-icons/Feather";
import Btn from "../components/Btn";

import auth from "@react-native-firebase/auth";
import { GoogleSignin } from "@react-native-community/google-signin";
import { LoginManager, AccessToken, GraphRequest, GraphRequestManager } from "react-native-fbsdk";
import { UsersServices } from "../utils/services/users";

const responseInfoCallback = async (error, result, token, navigation) => {
  console.log({ ...result, token }, "info request");

  if (error) {
    console.log("Error fetching data: " + error.toString());
  } else {
    navigation.navigate("RegisterGoogle", {
      responseUserGoogle: { idToken: token, user: { givenName: result.name, familyName: "", email: result.email } },
    });
    console.log("Success fetching data: " + result.toString());
  }
};
const getDataUser = async (token, navigation) => {
  const infoRequest = new GraphRequest("/me?fields=name,picture,email", null, (error, result) =>
    responseInfoCallback(error, result, token, navigation)
  );
  // Start the graph request.
  new GraphRequestManager().addRequest(infoRequest).start();
};

async function signInFacebook(navigation) {
  try {
    // Attempt login with permissions
    const result = await LoginManager.logInWithPermissions(["public_profile", "email"]);
    if (result.isCancelled) {
      throw "User cancelled the login process";
    }
    // Once signed in, get the users AccesToken
    const data = await AccessToken.getCurrentAccessToken();
    if (!data) {
      throw "Something went wrong obtaining access token";
    }
    // Create a Firebase credential with the AccessToken
    const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);
    // Sign-in the user with the credential
    await auth().signInWithCredential(facebookCredential);
    // Dispatch reducer
    await getDataUser(data.accessToken, navigation);
    console.log("Información FACEBOOK");
  } catch (error) {
    console.log(error, "este es el error");
  }
}

GoogleSignin.configure({
  webClientId: "1010142090014-7jv1kslmhhleorkqu0g07b1d1k45alcn.apps.googleusercontent.com",
});

const signInGoogle = async (navigation, dispatch) => {
  try {
    const userService = new UsersServices();
    await GoogleSignin.hasPlayServices({ showPlayServicesUpdateDialog: true });
    // Get the users ID token
    const responseUserGoogle = await GoogleSignin.signIn();
    const { idToken } = responseUserGoogle;
    // Create a Google credential with the token
    const googleCredential = auth.GoogleAuthProvider.credential(idToken);

    // Sign-in the user with the credential
    await auth().signInWithCredential(googleCredential);

    userService.setFilterWhereDefault("email", responseUserGoogle.user.email);
    const requestUser = await userService.getUsers();
    const user = requestUser.filter((x) => x.googelCode);

    if (user.length > 0) {
      dispatch(googleStart(user[0]));
    } else {
      navigation.navigate("RegisterGoogle", { responseUserGoogle });
    }

    // Dispatch reducer
  } catch (error) {
    console.log("error", error);
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow
    } else if (error.code === statusCodes.IN_PROGRESS) {
      // operation (e.g. sign in) is in progress already
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
    } else {
      // some other error happened
    }
  }
};
const getInputsForm = () => {
  return [
    {
      label: "Correo electronico",
      name: "email",
      icon: "user",
      defaultValue: "",
      //defaultValue: "diegochoqueramirez@gmail.com",
      rules: {
        required: { value: true, message: "Debes escribir un correo electronico" },
        pattern: {
          value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
          message: "Debe tener un formato de email valido",
        },
      },
    },
    {
      label: "Contraseña",
      name: "password",
      icon: "lock",
      iconRight: "eye",
      secureTextEntry: true,
      defaultValue: "",
      //defaultValue: "123456789",
      rules: {
        minLength: { value: 8, message: "Debe tener al menos 8 caracteres" },
        required: { value: true, message: "Debe escribir una contraseña" },
      },
    },
  ];
};

const signOut = async () => {
  try {
    await GoogleSignin.revokeAccess();
    await GoogleSignin.signOut();
  } catch (error) {
    console.error(error);
  }
};

const inputTheme = {
  placeholder: Theme.COLORS.SECONDARY,
  text: "black",
  primary: Theme.COLORS.PRIMARY,
};

export default function Login({ navigation }) {
  const {
    settings: { loader },
  } = useSelector((store) => store);
  const { control, handleSubmit, errors } = useForm();
  const inputsForm = useMemo(() => getInputsForm({ control }), []);
  const dispatch = useDispatch();

  useEffect(() => {
    signOut();
  }, []);

  const onSubmit = (data) => {
    dispatch(loginStart(data));
  };

  return (
    <View style={{ flex: 1, padding: 15, justifyContent: "center" }}>
      <Title title="Bienvenido!" color={Theme.COLORS.TITLE} />
      <SubTitle subTitle="Por favor ingrese su cuenta aquí" />
      {inputsForm.map((input, index) => (
        <ControllerTextInput
          key={index}
          {...input}
          errors={errors}
          control={control}
          render={({ onChange, onBlur, value }) => (
            <TextInput
              {...input}
              left={<TextInput.Icon name={() => <Icon name={input.icon} color={Theme.COLORS.SECONDARY} />} style={styles.icon} />}
              // right={<TextInput.Icon name={input.iconRight} style={styles.icon} />}
              style={styles.input}
              onBlur={onBlur}
              onChangeText={(value) => onChange(value)}
              value={value}
              error={errors[input.name]}
              theme={{ colors: inputTheme }}
              mode={"outlined"}
            />
          )}
        />
      ))}
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("RecoverPassword");
        }}
      >
        <Text style={{ textAlign: "right", marginVertical: 10 }}>Olvide mi contraseña</Text>
      </TouchableOpacity>
      <Btn loader={loader} title={"iniciar"} color={Theme.COLORS.PRIMARY} onPress={handleSubmit(onSubmit)} />
      <View style={{ justifyContent: "flex-end", marginVertical: 10, flexDirection: "row" }}>
        <Text>No tienes cuenta? </Text>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("Register");
          }}
        >
          <Text style={{ color: Theme.COLORS.PRIMARY }}>Registrate</Text>
        </TouchableOpacity>
      </View>
      <Text style={{ textAlign: "center", marginVertical: 20, color: Theme.COLORS.SECONDARY }}>O continuar con</Text>
      <View style={{ marginBottom: 20 }}>
        <Btn icon={"google"} title="google" color={Theme.COLORS.GOOGLE} onPress={() => signInGoogle(navigation, dispatch)} />
      </View>
      <Btn icon="facebook" title="facebook" color={Theme.COLORS.FACEBOOK} onPress={() => signInFacebook(navigation)} />
    </View>
  );
}

const styles = StyleSheet.create({
  touchRegistrate: {
    width: "100%",
    height: "3%",
  },
  input: {
    marginTop: 10,
    height: 50,
  },
  icon: {
    color: "red",
    marginTop: 15,
  },
});
