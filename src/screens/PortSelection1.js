import React, { useEffect, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Theme from '../utils/theme';
import DropDownPicker from 'react-native-dropdown-picker';
import { customUseReducer } from '../utils/customHooks';
import { HarborsServices } from '../utils/services/harbors';
import { SubTitle } from '../components';

var companies = [
  {
    id: '909111c4-9a2d-434b-b3eb-ed6c640e2936',
    priceRoute: 4000,
    originHarborId: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
    destinationHarborId: '3616b417-487b-4d1c-bc0f-3ed0a770d290',
    shippingCompanyId: 'a4e3fbaf-09a7-424c-aa57-5a0ac559cae3',
    presaleId: 'ff86e7f6-1694-4a86-87fc-c310ab899c65',
    containerId: '32e75bb7-d8ef-461c-a97c-d2706899bb1a',
    companyOriginHarbor: {
      id: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
      title: 'Concepcion',
      description: 'Puerto 6 origin en chile',
      harborType: true,
      countriesId: 'f4971fd7-9b1e-4e85-a261-5167c282632f',
      harborCountries: {
        id: 'f4971fd7-9b1e-4e85-a261-5167c282632f',
        country: 'Chile',
      },
    },
  },
  {
    id: 'b24debdb-0f8a-43a0-8b78-25decc2675b6',
    priceRoute: 4000,
    originHarborId: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
    destinationHarborId: '3616b417-487b-4d1c-bc0f-3ed0a770d290',
    shippingCompanyId: 'a4e3fbaf-09a7-424c-aa57-5a0ac559cae3',
    presaleId: 'ff86e7f6-1694-4a86-87fc-c310ab899c65',
    containerId: '32e75bb7-d8ef-461c-a97c-d2706899bb1a',
    companyOriginHarbor: {
      id: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
      title: 'Concepcion',
      description: 'Puerto 6 origin en chile',
      harborType: true,
      countriesId: 'f4971fd7-9b1e-4e85-a261-5167c282632f',
      harborCountries: {
        id: 'f4971fd7-9b1e-4e85-a261-5167c282632f',
        country: 'Chile',
      },
    },
  },
  {
    id: 'ca67a1ab-ce97-48e9-b426-ed1925bccb7f',
    priceRoute: 3300,
    originHarborId: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
    destinationHarborId: '3616b417-487b-4d1c-bc0f-3ed0a770d290',
    shippingCompanyId: 'a4e3fbaf-09a7-424c-aa57-5a0ac559cae3',
    presaleId: 'ff86e7f6-1694-4a86-87fc-c310ab899c65',
    containerId: '32e75bb7-d8ef-461c-a97c-d2706899bb1a',
    companyOriginHarbor: {
      id: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
      title: 'Concepcion',
      description: 'Puerto 6 origin en chile',
      harborType: true,
      countriesId: 'f4971fd7-9b1e-4e85-a261-5167c282632f',
      harborCountries: {
        id: 'f4971fd7-9b1e-4e85-a261-5167c282632f',
        country: 'Chile',
      },
    },
  },
  {
    id: 'ca67a1ab-ce97-48e9-b426-ed1925bccb7f',
    priceRoute: 3300,
    originHarborId: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
    destinationHarborId: '3616b417-487b-4d1c-bc0f-3ed0a770d290',
    shippingCompanyId: 'a4e3fbaf-09a7-424c-aa57-5a0ac559cae3',
    presaleId: 'ff86e7f6-1694-4a86-87fc-c310ab899c65',
    containerId: '32e75bb7-d8ef-461c-a97c-d2706899bb1a',
    companyOriginHarbor: {
      id: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
      title: 'Shangai',
      description: 'Puerto 6 origin en chile',
      harborType: true,
      countriesId: '123',
      harborCountries: {
        id: '123',
        country: 'China',
      },
    },
  },
  {
    id: 'ca67a1ab-ce97-48e9-b426-ed1925bccb7f',
    priceRoute: 3300,
    originHarborId: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
    destinationHarborId: '3616b417-487b-4d1c-bc0f-3ed0a770d290',
    shippingCompanyId: 'a4e3fbaf-09a7-424c-aa57-5a0ac559cae3',
    presaleId: 'ff86e7f6-1694-4a86-87fc-c310ab899c65',
    containerId: '32e75bb7-d8ef-461c-a97c-d2706899bb1a',
    companyOriginHarbor: {
      id: '03196504-9a0d-4ef8-84c8-d0d33f05b931',
      title: 'Tokio',
      description: 'Puerto 6 origin en chile',
      harborType: true,
      countriesId: '123',
      harborCountries: {
        id: '123',
        country: 'China',
      },
    },
  },
];

const initialState = {
  originRequest: [],
  originRequestHarbor: [],
  destinationRequest: [],
  destinationRequestHarbor: [],
};

export default function PortSelection1({ state, dispatchComponent, presale }) {
  const harborServices = new HarborsServices();
  const [request, setRequest] = customUseReducer(initialState);
  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    harborServices.setFilterWhereDefault('shippingCompanyId', state.shippingCompany.id);
    const requestStartHarbors = await harborServices.getOriginHarbors();
    const requestEndHarbors = await harborServices.getDepartureHarbors();

    let resultOriginHarbor = await requestStartHarbors.filter((item, key) => item.companyOriginHarbor);
    let resultDestinationHarbor = await requestEndHarbors.filter((item, key) => item.companyDestinationHarbor);

    setRequest({
      originRequest: resultOriginHarbor.map((item, key) => item.companyOriginHarbor),
      destinationRequest: resultDestinationHarbor.map((item, key) => item.companyDestinationHarbor),
    });

    let hashOrigin = {};
    let resultOrigin = resultOriginHarbor
      .map((item, key) => item.companyOriginHarbor.harborCountries)
      .filter((element) => (hashOrigin[element.id] ? false : (hashOrigin[element.id] = true)));

    let hashDestination = {};
    let resultDestination = resultDestinationHarbor
      .map((item, key) => item.companyDestinationHarbor.harborCountries)
      .filter((element) => (hashDestination[element.id] ? false : (hashDestination[element.id] = true)));
    dispatchComponent({
      originCountries: resultOrigin,
      destinationCountries: resultDestination,
    });
  };

  const getOriginCountries = (item) => {
    let hash = {};
    let result = request.originRequest
      .filter((ele) => ele.countriesId === item.value)
      .filter((element) => (hash[element.id] ? false : (hash[element.id] = true)));

    setRequest({
      originRequestHarbor: result,
    });
  };

  const getOriginHarbor = (item) => {
    dispatchComponent({
      startHarbor: { id: item.value, name: item.label },
    });
  };

  const getDepartureCountries = (item) => {
    let hash = {};
    let result = request.destinationRequest
      .filter((ele) => ele.countriesId === item.value)
      .filter((element) => (hash[element.id] ? false : (hash[element.id] = true)));

    setRequest({
      destinationRequestHarbor: result,
    });
  };

  const getDepartureHarbor = (item) => {
    dispatchComponent({
      endHarbor: { id: item.value, name: item.label },
    });
  };

  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 20,
      }}
    >
      <SubTitle subTitle={'Elija las características que necesita, para sus puertos.'} />
      <View style={{ flexDirection: 'row', marginTop: 20, flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.textTitle}>Pais</Text>
          <DropDownPicker
            items={state.originCountries.map((item) => ({
              value: item.id,
              label: item.country,
            }))}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: Theme.COLORS.PRIMARY, width: '80%' }}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            placeholder={'Seleccione'}
            labelStyle={{ color: Theme.COLORS.WHITE }}
            dropDownStyle={{
              backgroundColor: Theme.COLORS.PRIMARY,
              width: '80%',
            }}
            onChangeItem={getOriginCountries}
          />
        </View>
        <View style={{ flex: 1.2 }}>
          <Text style={styles.textTitle}>Puertos de Origen</Text>
          <DropDownPicker
            items={request.originRequestHarbor.map((item) => ({
              value: item.id,
              label: item.title,
            }))}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: Theme.COLORS.PRIMARY, width: '100%' }}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            placeholder={'Seleccione'}
            labelStyle={{ color: Theme.COLORS.WHITE }}
            dropDownStyle={{
              backgroundColor: Theme.COLORS.PRIMARY,
              width: '100%',
            }}
            onChangeItem={getOriginHarbor}
          />
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 20, flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.textTitle}>Pais</Text>
          <DropDownPicker
            items={state.destinationCountries.map((item) => ({
              value: item.id,
              label: item.country,
            }))}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: Theme.COLORS.PRIMARY, width: '80%' }}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            placeholder={'Seleccione'}
            labelStyle={{ color: Theme.COLORS.WHITE }}
            dropDownStyle={{
              backgroundColor: Theme.COLORS.PRIMARY,
              width: '80%',
            }}
            onChangeItem={getDepartureCountries}
          />
        </View>
        <View style={{ flex: 1.2 }}>
          <Text style={styles.textTitle}>Puertos de Destino</Text>
          <DropDownPicker
            items={request.destinationRequestHarbor.map((item) => ({
              value: item.id,
              label: item.title,
            }))}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: Theme.COLORS.PRIMARY, width: '100%' }}
            itemStyle={{
              justifyContent: 'flex-start',
            }}
            placeholder={'Seleccione'}
            labelStyle={{ color: Theme.COLORS.WHITE }}
            dropDownStyle={{
              backgroundColor: Theme.COLORS.PRIMARY,
              width: '100%',
            }}
            onChangeItem={getDepartureHarbor}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    alignSelf: 'center',
  },
  viewText: {
    marginTop: '14%',
    marginEnd: 12,
  },
  stext: {
    color: '#3E5481',
    fontSize: 21,
  },
  sres: {
    color: '#1FCC79',
    fontSize: 21,
  },
  sres1: {
    color: '#EB5757',
    fontSize: 21,
  },
  textTitle: {
    color: '#3E5481',
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
  contSelect: {
    marginEnd: 70,
    marginTop: 20,
  },
  contSelect2: {
    marginEnd: 70,
    marginTop: 60,
  },
  viewContainer: {
    /* backgroundColor:'blue', */
  },
  btnNext: {
    marginTop: '30%',
  },
  tipesContainers: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#1FCC79',
    marginTop: 12,
  },
});
