import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { Header } from "../components";
import { useDispatch, useSelector } from "react-redux";
import Theme from "../utils/theme";
import { ProgressSteps, ProgressStep } from "react-native-progress-steps";
import { PortSelection1, PortSelection2, PortSelection3, Providers } from ".";
import { INITIAL_REQUEST_START } from "../redux/constants";
import { customUseReducer } from "../utils/customHooks";
import { useForm } from "react-hook-form";
import { CompanyHarborsServices } from "../utils/services/companyHarbors";
import { ActivityIndicator } from "react-native-paper";
import { providerStart } from "../redux/actions";
import { PresalesServices } from "../utils/services/presales";
import moment from "moment";

const initialState = {
  shippingCompany: null,
  departureDate: [],
  destinationDate: null,
  originCountries: [],
  destinationCountries: [],
  startHarbor: null,
  endHarbor: null,
  container: null,
  provider: {},
  presale: null,
  loading: false,
  current: 1,
};

const fieldsValidate = {
  1: { shippingCompany: null, destinationDate: null },
  2: { startHarbor: null, endHarbor: null },
  3: { container: null },
};

export default function PortSelection({ navigation, route }) {
  const { presale } = route.params;
  const {
    settings: { loader },
    offers: { data },
  } = useSelector((store) => store);

  const dispatch = useDispatch();
  const { handleSubmit, control, errors } = useForm();
  const [state, dispatchComponent] = customUseReducer(initialState);
  const dates = data[0]?.presaleCompany?.shippingCompanyDepartureDate
    .map((item) => item.rangeDate)
    .reduce((el, acc) => el.concat(acc))
    .filter((item) => moment(item).isAfter());
  const convertDates = dates.map((item) => moment(item).format("DD/MM/YYYY"));

  useEffect(() => {
    dispatch({ type: INITIAL_REQUEST_START });
    presale &&
      dispatchComponent({
        shippingCompany: { id: data[0].presaleCompany.id, name: data[0].presaleCompany.name },
        departureDate: convertDates,
        container: { id: data[0].container.id, name: data[0].container.dimension },
      });
  }, []);

  const handleNext = () => {
    if (!getValidate() && Object.keys(fieldsValidate).length !== state.current) {
      setTimeout(() => {
        dispatchComponent((state) => ({
          ...state,
          current: state.current + 1,
        }));
      }, 200);
    }
  };

  const getValidate = () => {
    return Object.keys(fieldsValidate[state.current]).reduce((acc, el) => {
      const fieldStatus = state[el] === fieldsValidate[state.current][el];
      return fieldStatus ?? acc;
    }, false);
  };

  const { shippingCompany, destinationDate, startHarbor, endHarbor, container, provider, presale: Presale } = state;
  // console.log(
  //   shippingCompany,
  //   "SHIPPINGCOMPANY",
  //   destinationDate,
  //   "DESTINATIONDATE",
  //   startHarbor,
  //   "STARTHARBOR",
  //   endHarbor,
  //   "ENDHARBOR",
  //   container,
  //   "CONTAINER",
  //   provider,
  //   "PROVIDER",
  //   Presale,
  //   "PRESALE"
  // );

  const onSubmit = async (form) => {
    let newState = {};
    const presaleService = new PresalesServices();
    const requestPresaleId = await presaleService.getPresaleId(state);

    await dispatchComponent((state) => {
      newState = {
        ...state,
        provider: form,
        presale:
          requestPresaleId[0].presaleId == data[0].id
            ? {
                id: data[0].id,
                price: data[0].discountedPrice,
              }
            : {
                id: requestPresaleId[0].presaleId,
                price: requestPresaleId[0].priceRoute,
              },
      };
      return newState;
    });
    navigation.navigate("ShoppingHistory", { newState });
  };

  return (
    <View style={styles.container}>
      <Header title="Formulario" noMenu color="#3E5481" navigation={navigation} />
      <ProgressSteps
        completedProgressBarColor={Theme.COLORS.PRIMARY}
        completedStepIconColor={Theme.COLORS.PRIMARY}
        activeStepIconColor={Theme.COLORS.WHITE}
        activeLabelColor={Theme.COLORS.PRIMARY}
        activeStepIconBorderColor={Theme.COLORS.PRIMARY}
        borderWidth={3}
      >
        <ProgressStep
          scrollable={false}
          nextBtnTextStyle={styles.text}
          nextBtnStyle={styles.btnNext}
          nextBtnText={"Siguiente"}
          label="Naviera"
          errors={getValidate()}
          onNext={handleNext}
        >
          <View style={{ height: "80%", justifyContent: "center" }}>
            {loader ? (
              <ActivityIndicator animating={true} color={Theme.COLORS.PRIMARY} />
            ) : (
              <PortSelection3 state={state} dispatchComponent={dispatchComponent} presale={presale} />
            )}
          </View>
        </ProgressStep>
        <ProgressStep
          scrollable={false}
          previousBtnTextStyle={styles.textPrevious}
          previousBtnStyle={styles.btnPrevious}
          nextBtnTextStyle={styles.text}
          nextBtnStyle={styles.btnNext}
          nextBtnText={"Siguiente"}
          previousBtnText={"Anterior"}
          label="Puertos"
          errors={getValidate()}
          onNext={handleNext}
        >
          <View style={{ height: "80%" }}>
            <PortSelection1 state={state} dispatchComponent={dispatchComponent} presale={presale} />
          </View>
        </ProgressStep>

        <ProgressStep
          scrollable={false}
          previousBtnTextStyle={styles.textPrevious}
          previousBtnStyle={styles.btnPrevious}
          nextBtnTextStyle={styles.text}
          nextBtnStyle={styles.btnNext}
          nextBtnText={"Siguiente"}
          previousBtnText={"Anterior"}
          label="Contenedores"
          errors={getValidate()}
          onNext={handleNext}
        >
          <View style={{ height: "80%" }}>
            <PortSelection2 state={state} dispatchComponent={dispatchComponent} presale={presale} />
          </View>
        </ProgressStep>
        <ProgressStep
          scrollable={true}
          previousBtnTextStyle={styles.textPrevious}
          previousBtnStyle={styles.btnPrevious}
          nextBtnTextStyle={styles.text}
          nextBtnStyle={styles.btnNext}
          nextBtnText={"Siguiente"}
          previousBtnText={"Anterior"}
          finishBtnText={"Finalizar"}
          onSubmit={handleSubmit(onSubmit)}
          label="Proveedor"
        >
          <View style={{ height: "80%" }}>
            <Providers state={state} dispathComponent={dispatchComponent} handleSubmit={handleSubmit} control={control} errors={errors} />
          </View>
        </ProgressStep>
      </ProgressSteps>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "white",
  },
  btnNext: {
    justifyContent: "center",
    alignItems: "center",
  },
  btnPrevious: {
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: Theme.COLORS.PRIMARY,
    flex: 1,
  },
  text: {
    color: Theme.COLORS.FORM,
    fontWeight: "bold",
    fontSize: 20,
  },
  textPrevious: {
    color: Theme.COLORS.FORM,
    fontWeight: "bold",
    fontSize: 20,
  },
});
