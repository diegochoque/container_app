import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, Alert } from "react-native";
import { ActivityIndicator } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { Header, SubTitle, Title } from "../components";
import { providerStart } from "../redux/actions";
import theme from "../utils/theme";

export default function Payment({ navigation, route }) {
  const {
    settings: { loader },
  } = useSelector((store) => store);
  const { newState } = route.params;
  const dispatch = useDispatch();

  const onSubmit = (method) => {
    return new Promise((resolve, reject) => {
      dispatch(providerStart({ newState, resolve, reject, method }));
    })
      .then((response) => {
        Alert.alert(
          "Estado del pago",
          `Por favor realice el depósito en el banco con este código de trámite: ${response.recaudationCode}`,
          [
            {
              text: "Aceptar",
              onPress: () => navigation.navigate("Offers"),
            },
          ],
          { cancelable: false }
        );
      })
      .catch(console.warn);
  };

  return (
    <View style={{ flex: 1, padding: "5%" }}>
      <Header title="Pagos" noMenu color="#3E5481" navigation={navigation} />
      <SubTitle subTitle={"Elija la opcion de pagos de su preferencia"} />
      <Title title={`Monto a pagar: ${newState.presale.price} $`} color={theme.COLORS.TITLE} />
      <View style={{ flex: 1, justifyContent: "center" }}>
        <TouchableOpacity
          style={{
            height: "25%",
            backgroundColor: "#003396",
            marginTop: "-10%",
            borderRadius: 10,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 8,
            },
            shadowOpacity: 0.46,
            shadowRadius: 11.14,

            elevation: 17,
          }}
          onPress={() => {
            Alert.alert(
              "Deposito por tarjeta",
              `Aun no se encuentra disponible el deposito con tarjeta`,
              [
                {
                  text: "Aceptar",
                },
              ],
              { cancelable: true }
            );
          }}
        >
          <View style={{ height: 50, width: 100 }}>
            <Image source={require("../../assets/images/visa.png")} style={{ flex: 1, width: null, height: null, resizeMode: "contain" }} />
          </View>
          <View style={{ justifyContent: "center", alignItems: "center", flex: 1, marginTop: -50 }}>
            <Text style={{ color: theme.COLORS.WHITE, fontSize: 20 }}>Deposito por tarjeta</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            height: "25%",
            backgroundColor: "#003396",
            marginTop: "10%",
            borderRadius: 10,
            shadowColor: "#000",
            shadowOffset: {
              width: 0,
              height: 8,
            },
            shadowOpacity: 0.46,
            shadowRadius: 11.14,

            elevation: 17,
          }}
          onPress={() => onSubmit("Banco")}
        >
          <View style={{ justifyContent: "center", alignItems: "center", flex: 1 }}>
            {loader ? (
              <ActivityIndicator animating={true} color={theme.COLORS.WHITE} />
            ) : (
              <Text style={{ color: theme.COLORS.WHITE, fontSize: 20 }}>Deposito por banco</Text>
            )}
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    alignSelf: "center",
  },
  viewText: {
    marginTop: "14%",
    marginEnd: 12,
  },
  stext: {
    color: "#3E5481",
    fontSize: 21,
  },
  sres: {
    color: "#1FCC79",
    fontSize: 21,
  },
  sres1: {
    color: "#EB5757",
    fontSize: 21,
  },
});
