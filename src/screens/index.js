//import Retrieve from "./Retrieve";
import Login from "./Login";
import Register from "./Register";
import Register2 from "./Register2";
import RecoverPassword from "./RecoverPassword";
import PasswordVerification from "./PasswordVerification";
import Offers from "./Offers";
import ShoppingHistory from "./ShoppingHistory";
import PortSelection from "./PortSelection";
import PortSelection1 from "./PortSelection1";
import PortSelection2 from "./PortSelection2";
import PortSelection3 from "./PortSelection3";
import Providers from "./Providers";
import PaymentHistory from "./PaymentHistory";
import RegisterGoogle from "./RegisterGoogle";
import Profile from "./Profile";

export {
  //History,
  Login,
  Offers,
  Register,
  Register2,
  RecoverPassword,
  PasswordVerification,
  ShoppingHistory,
  PortSelection,
  PortSelection1,
  PortSelection2,
  PortSelection3,
  Providers,
  PaymentHistory,
  RegisterGoogle,
  Profile,
};
