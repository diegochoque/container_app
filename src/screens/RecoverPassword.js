import React, { useState, useEffect, useMemo } from 'react';
import IconButton from '../components/IconButton';
import { useForm } from 'react-hook-form';
import ControllerTextInput from '../components/ControllerTextInput';
import { TextInput, Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Feather';
import { recoverStart } from '../redux/actions';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
} from 'react-native';
import {
    ScreenContainer,
    ContentCard,
    Input,
    Title,
} from '../components';
import Btn from '../components/Btn';
import { useDispatch, useSelector } from 'react-redux';
import Theme from '../utils/theme';

import {
    validateEmail,
    validateRequired,
    validateForm,
    parseForm,
} from '../utils/authValidation';
import SubTitle from '../components/Subtitle';
import { ScrollView } from 'react-native-gesture-handler';

const getInputsForm = () => {
    return [
        {
            label: 'Correo electronico',
            name: 'email',
            icon: 'user',
            defaultValue: 'gutierrez201801@hotmail.com',
            rules: {
                required: { value: true, message: 'Debes escribir un correo electronico' },
                pattern: {
                    value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
                    message: 'Debe tener un formato de email valido',
                },
            },
        },
    ]
};

const inputTheme = {
    placeholder: Theme.COLORS.SECONDARY,
    text: 'black',
    primary: Theme.COLORS.PRIMARY,
};

export default function RecoverPassword({ navigation }) {

    const {
        settings: { loader },
    } = useSelector((store) => store);
    const { control, handleSubmit, errors } = useForm();
    const inputsForm = useMemo(() => getInputsForm({ control }), []);
    const dispatch = useDispatch();

    const onSubmit = (data) => {
        dispatch(recoverStart({ data, navigation }));
    };


    const storage = useSelector((store) => store);


    return (
        <View style={{ flex: 1, padding: 15, justifyContent: 'center' }}>

            <IconButton
                name="keyboard-arrow-left"
                family="MaterialIcons"
                size={40}
                marginR={330}
                onPress={navigation.goBack}
            />
            <Title
                title="Recuperar Contraseña"
                color="#2E3E5C"
                marginT={80}>
            </Title>
            <SubTitle style={{ justifyContent: "center" }}
                subTitle="Ingrese su correo electrónico para recuperar su contraseña"
                color="#9FA5C0"
                marginT={20}
            >
            </SubTitle>

            <ScrollView>
                {inputsForm.map((input, index) => (
                    <ControllerTextInput
                        key={index}
                        {...input}
                        errors={errors}
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                            <TextInput
                                {...input}
                                left={
                                    <TextInput.Icon
                                        name={() => <Icon name={input.icon} color={Theme.COLORS.SECONDARY} />}
                                        style={styles.icon}
                                    />
                                }
                                // right={<TextInput.Icon name={input.iconRight} style={styles.icon} />}
                                style={styles.input}
                                onBlur={onBlur}
                                onChangeText={(value) => onChange(value)}
                                value={value}
                                error={errors[input.name]}
                                theme={{ colors: inputTheme }}
                                mode={'outlined'}
                            />
                        )}
                    />
                ))}

                <View style={{ marginTop: 20 }}>

                    <Btn loader={loader} title={'Enviar'} color={Theme.COLORS.PRIMARY} onPress={handleSubmit(onSubmit)} />
                </View>
            </ScrollView>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        alignSelf: 'center',
    },
    buttonForgotPassword: {
        margin: 0,
        width: 175,
    },
    stylesTextButton: {
        fontSize: 20,
        color: 'white',
    },
    stylesView: {
        marginTop: 20,
        width: '100%',
        alignItems: 'center',
    },
    stylesInput: {
        width: '100%',
        borderRadius: 15,
        borderRadius: 70 / 2,
        backgroundColor: '#FFFFFF',
        height: 50,
        padding: 5,
        fontSize: 20,
        justifyContent: 'center',
        alignContent: 'center',
    },
    btnGoogle: {
        backgroundColor: '#FF5842'
    },
    btnFacebook: {
        backgroundColor: '#4267B2'
    },
    touchRegistrate: {
        width: '100%',
        height: '3%',
    },
    input: {
        marginTop: 10,
        height: 50,
    },
    icon: {
        color: 'red',
        marginTop: 15,
    },
});
