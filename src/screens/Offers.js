import React, { useEffect } from "react";
import IconButton from "../components/IconButton";
import { StyleSheet, Text, Image, View, TouchableOpacity } from "react-native";
import { ScreenContainer, ContentCard, Button, Title } from "../components";
import { useDispatch, useSelector } from "react-redux";
import Theme from "../utils/theme";
import { offersStart } from "../redux/actions";
import moment from "moment";

export default function Offers(props) {
  const { navigation } = props;
  const dispatch = useDispatch();
  const { offers } = useSelector((store) => store);
  const offerSelected = offers.data.length > 0 ? offers.data[0] : {};
  const startDate = offerSelected.startDate ? moment(offerSelected.startDate) : moment();
  const endDate = offerSelected.endDate ? moment(offerSelected.endDate) : moment();

  useEffect(() => {
    dispatch(offersStart());
  }, []);

  return (
    <ScreenContainer color="white" style={styles.container} position="flex-end">
      <IconButton
        name="menu"
        family="MaterialIcons"
        size={40}
        marginT={25}
        marginR={300}
        /* iconColor={color} */
        /*  color={backgroundColor} */
        onPress={() => navigation.openDrawer()}
      />
      <View style={styles.header}>
        <Title title="Ofertas" color="#2E3E5C"></Title>
      </View>

      <ContentCard color={Theme.COLORS.RED}>
        <Image style={styles.offerpng} source={require("../../assets/images/offer4.png")}></Image>

        <View style={styles.home}>
          <Text style={styles.apro}>{offerSelected.title}</Text>
          <Text style={styles.text1}>{offerSelected.description}</Text>
          <Text style={styles.desc}> {offerSelected.discountRate}% </Text>
          <Text style={styles.text1}>
            Carga tus mercaderías entre el
            <Text style={{ color: "#FAFF00" }}>{` ${startDate.format("DD")} de ${startDate.format("MMM")} `}</Text>
            al
            <Text style={{ color: "#FAFF00" }}>{` ${endDate.format("DD")} de ${endDate.format("MMM")} `} </Text>
          </Text>
          <Text style={styles.text1}>Escoge el puerto de tu preferencia!</Text>
          <Text style={styles.text1}>Por si esto fuera poco, estarás con la mejor naviera</Text>
          <Text style={styles.textYellow}>{offerSelected?.presaleCompany?.name}</Text>
          <Text style={styles.textYellow}>!Todo esto a tan solo!</Text>
          <Text style={styles.price}>{`${offerSelected.discountedPrice}$`}</Text>
        </View>
        <Button
          style={styles.btnGet}
          small={true}
          round={true}
          shadowless={true}
          onPress={() => navigation.navigate("PortSelection", { presale: true })}
        >
          <Text style={styles.textGet}>Obtener</Text>
        </Button>
        <View style={styles.line} />
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("PortSelection", { presale: false });
          }}
        >
          <Text style={styles.noThanks}>No Gracias</Text>
        </TouchableOpacity>
      </ContentCard>
    </ScreenContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    alignSelf: "center",
  },
  stylesView: {
    marginTop: 20,
    width: "100%",
    alignItems: "center",
  },
  stylesInput: {
    width: "100%",
    borderRadius: 15,
    borderRadius: 70 / 2,
    backgroundColor: "#FFFFFF",
    height: 50,
    padding: 5,
    fontSize: 20,
    justifyContent: "center",
    alignContent: "center",
  },
  btnGet: {
    backgroundColor: "#FFFFFF",
    marginTop: "3%",
  },
  textGet: {
    color: "#FF0000",
    fontWeight: "bold",
    fontSize: 23,
  },
  noThanks: {
    color: "white",
    fontWeight: "bold",
    fontSize: 23,
    marginTop: "7%",
  },
  line: {
    width: "100%",
    height: "1%",
    backgroundColor: "white",
    marginTop: "5%",
  },
  touchRegistrate: {
    width: "100%",
    height: "3%",
  },
  header: {
    marginTop: -30,
    marginBottom: 20,
  },
  imagen: {
    marginEnd: "69%",
    marginTop: -20,
  },
  offerpng: {
    width: 120,
    height: 120,
    marginEnd: "69%",
    marginTop: -20,
  },
  apro: {
    color: "white",
    fontSize: 23,
    fontWeight: "bold",
    textAlign: "center",
    marginLeft: 50,
    marginRight: 60,
  },
  text1: {
    color: "white",
    fontSize: 22,
    fontWeight: "bold",
    textAlign: "center",
    marginLeft: 40,
    marginRight: 50,
  },
  textYellow: {
    color: "#FAFF00",
    fontSize: 22,
    fontWeight: "bold",
    textAlign: "center",
    marginLeft: 40,
    marginRight: 50,
  },
  price: {
    color: "white",
    fontSize: 24,
    fontWeight: "bold",
    textAlign: "center",
    marginLeft: 40,
    marginRight: 50,
  },
  desc: {
    color: "white",
    fontSize: 53,
    fontWeight: "bold",
  },
  home: {
    alignItems: "center",
    marginTop: -62,
  },
});
