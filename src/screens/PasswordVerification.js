import React, { useState, useEffect } from 'react';
import { Icon } from "galio-framework";
import IconButton from '../components/IconButton';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
} from 'react-native';
import {
    ScreenContainer,
    ContentCard,
    Button,
    Checkbox,
    Input,
    Title,
    InputSquare,
} from '../components';
import { useDispatch, useSelector } from 'react-redux';
import Theme from '../utils/theme';

import {
    validateEmail,
    validateRequired,
    validateForm,
    parseForm,
} from '../utils/authValidation';
import SubTitle from '../components/Subtitle';
import theme from '../utils/theme';
//import {fetchPayments} from '../store/actions/payments';
export default function PasswordVerification({ navigation }) {
    const storage = useSelector((store) => store);
    return (
        <ScreenContainer
            color="white"
            style={styles.container}
            position="flex-end">

            <ContentCard
                color={Theme.COLORS.WHITE}>
                <IconButton
                    name="keyboard-arrow-left"
                    family="MaterialIcons"
                    size={40}
                    marginR={330}
                //   iconColor={color}
                /*color={backgroundColor}
                onPress={() => navigation.goBack()} */
                />
                <Title
                    title="Revisa Tu Correo"
                    color="#2E3E5C"
                    marginT={20}>
                </Title>
                <SubTitle
                    subTitle="Enviamos el código a tu correo electrónico"
                    color="#9FA5C0">
                </SubTitle>
                <Text></Text>
                <View style={{ flexDirection: 'row' }}>
                    <InputSquare
                        shadowles
                        round
                        placeholder=""
                    />
                    <InputSquare
                        shadowles
                        round
                        placeholder=""
                    />
                    <InputSquare
                        shadowles
                        round
                        placeholder=""
                    />
                    <InputSquare
                        shadowles
                        round
                        placeholder=""
                    />
                </View>
                <SubTitle
                    subTitle="Código expira en : 03:12"
                    marginT={25}
                    color="#2E3E5C"
                    /* marginL={190} */>
                </SubTitle>
                <Text></Text>
                <Button
                    color="primary"
                    normal={true}
                    round={true}
                    shadowless={true}
                    onPress={() => {
                    }
                    }>Siguiente</Button>
                <Button
                    style={styles.btnGoogle}
                    normal={true}
                    round={true}
                    shadowless={true}
                    onPress={() => { navigation.navigate() }}>
                    <Text style={{ color: "#9FA5C0" }}>Enviar De Nuevo</Text>
                </Button>

            </ContentCard>
        </ScreenContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        alignSelf: 'center',
    },
    buttonForgotPassword: {
        margin: 0,
        width: 175,
    },
    stylesTextButton: {
        fontSize: 20,
        color: 'white',
    },
    stylesView: {
        marginTop: 20,
        width: '100%',
        alignItems: 'center',
    },
    stylesInput: {
        width: '100%',
        borderRadius: 15,
        borderRadius: 70 / 2,
        backgroundColor: '#FFFFFF',
        height: 50,
        padding: 5,
        fontSize: 20,
        justifyContent: 'center',
        alignContent: 'center',
    },
    btnGoogle: {
        backgroundColor: '#FFFFFF',
        borderWidth: 2,
        borderColor: "#D0DBEA"
    },
    btnFacebook: {
        backgroundColor: '#4267B2',
        borderWidth: 2,
        borderColor: "#D0DBEA"
    },
    touchRegistrate: {
        width: '100%',
        height: '3%',
    }

});
