import React, { useState, useEffect } from 'react';
import { Icon } from "galio-framework";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
} from 'react-native';
import {
    ScreenContainer,
    ContentCard,
    Button,
    Checkbox,
    Input,
    Title,
    Select,
} from '../components';
import { useDispatch, useSelector } from 'react-redux';
import Theme from '../utils/theme';

import {
    validateEmail,
    validateRequired,
    validateForm,
    parseForm,
} from '../utils/authValidation';
import SubTitle from '../components/Subtitle';
import theme from '../utils/theme';
//import {fetchPayments} from '../store/actions/payments';
export default function Register2({ navigation }) {
    const storage = useSelector((store) => store);
    return (
        <ScreenContainer
            color="white"
            style={styles.container}
            position="flex-end">

            <ContentCard
                color={Theme.COLORS.WHITE}>
                <Title
                    title="Últimos Pasos!"
                    color="#2E3E5C">
                </Title>
                <SubTitle
                    subTitle="Por favor ingrese los últimos datos aquí"
                    color="#9FA5C0">
                </SubTitle>
                <Text></Text>
                <Text></Text>
                <Text style={styles.number}>Número Whatsapp/WeChat</Text>
                <Input
                    shadowles
                    round
                    placeholder="Número" />
                <Text style={styles.email}>Correo Electrónico</Text>
                <Input
                    round
                    placeholder="Correo electrónico" />
                <Text style={styles.pass}>Contraseña</Text>

                <Input
                    round
                    password
                    viewPass
                    placeholder="Contraseña" />

                <Button
                    color="primary"
                    normal={true}
                    round={true}
                    shadowless={true}
                    onPress={() => {
                    }
                    }>Finalizar </Button>

            </ContentCard>
        </ScreenContainer>
    );
}

const styles = StyleSheet.create({
    container: {
        height: '100%',
        alignSelf: 'center',
    },
    buttonForgotPassword: {
        margin: 0,
        width: 175,
    },
    stylesTextButton: {
        fontSize: 20,
        color: 'white',
    },
    stylesView: {
        marginTop: 20,
        width: '100%',
        alignItems: 'center',
    },
    stylesInput: {
        width: '100%',
        borderRadius: 15,
        borderRadius: 70 / 2,
        backgroundColor: '#FFFFFF',
        height: 50,
        padding: 5,
        fontSize: 20,
        justifyContent: 'center',
        alignContent: 'center',
    },
    btnGoogle: {
        backgroundColor: '#FF5842'
    },
    btnFacebook: {
        backgroundColor: '#4267B2'
    },
    number: {
        marginRight: '34%',
        color: '#2E3E5C'
    },
    email: {
        marginRight: '50%',
        color: '#2E3E5C'
    },
    pass: {
        marginEnd: '60%',
        color: '#2E3E5C'
    }

});
