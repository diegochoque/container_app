import React from "react";
import { StyleSheet, View } from "react-native";
import { Header, SubTitle, Title } from "../components";
import Btn from "../components/Btn";
import theme from "../utils/theme";

export default function ShoppingHistory({ navigation, route }) {
  const { newState } = route.params;

  return (
    <View style={{ flex: 1, padding: "5%" }}>
      <Header title="Detalles" noMenu color="#3E5481" navigation={navigation} />
      <SubTitle subTitle={"Por favor revise la informacion, si todo esta correcto, puede realizar el pago"} />

      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <Title title="Puerto de origen: " color={theme.COLORS.TITLE}></Title>
        <Title title={newState.startHarbor.name} color={theme.COLORS.PRIMARY} />
        <Title title="Puerto de destino: " color={theme.COLORS.TITLE}></Title>
        <Title title={newState.endHarbor.name} color={theme.COLORS.PRIMARY} />
        <Title title="Tamano del contenedor: " color={theme.COLORS.TITLE}></Title>
        <Title title={newState.container.name} color={theme.COLORS.PRIMARY} />
        <Title title="Fecha de salida: " color={theme.COLORS.TITLE}></Title>
        <Title title={newState.destinationDate} color={theme.COLORS.PRIMARY} />
        <Title title="Naviera elegida: " color={theme.COLORS.TITLE}></Title>
        <Title title={newState.shippingCompany.name} color={theme.COLORS.PRIMARY} />
        <Title title="Precio Total: " color={theme.COLORS.TITLE}></Title>
        <Title title={`${newState.presale.price} $`} color={theme.COLORS.RED} />
      </View>
      <View style={{ marginTop: 50 }}>
        <Btn title="pagar" color={theme.COLORS.GOOGLE} onPress={() => navigation.navigate("Payment", { newState })} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    alignSelf: "center",
  },
  viewText: {
    marginTop: "14%",
    marginEnd: 12,
  },
  stext: {
    color: "#3E5481",
    fontSize: 21,
  },
  sres: {
    color: "#1FCC79",
    fontSize: 21,
  },
  sres1: {
    color: "#EB5757",
    fontSize: 21,
  },
});
