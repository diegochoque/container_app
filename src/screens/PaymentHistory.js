import React, { useEffect } from "react";
import { ActivityIndicator, ScrollView, StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { Header } from "../components";
import HistoryItem from "../components/HistoryItem";
import { requestPaymentsUsersStart } from "../redux/actions";
import theme from "../utils/theme";

const PaymentHistory = ({ navigation }) => {
  const dispatch = useDispatch();
  const {
    payment: { data },
    settings,
  } = useSelector((store) => store);

  useEffect(() => {
    dispatch(requestPaymentsUsersStart());
  }, []);

  return (
    <View style={styles.container}>
      <Header title="Historial de Pagos" noMenu color="#3E5481" navigation={navigation} />
      {settings.loader && <ActivityIndicator color="black" />}
      <ScrollView>
        {data.length > 0 ? (
          data.map((item, key) => <HistoryItem {...item} key={key} />)
        ) : (
          <View style={{ justifyContent: "center", alignItems: "center", height: "100%" }}>
            <Text style={{ textAlign: "center", fontSize: 18, fontWeight: "bold", color: theme.COLORS.TITLE }}>No tiene pedidos aun</Text>
          </View>
        )}
      </ScrollView>
      <View style={{ flexDirection: "row" }}>
        <View></View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
});

export default PaymentHistory;
