import React, { useEffect, useMemo } from "react";
import { useForm } from "react-hook-form";
import { StyleSheet, Text, View } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import { ScrollView } from "react-native-gesture-handler";
import { TextInput, Button } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { Header } from "../components";
import ControllerTextInput from "../components/ControllerTextInput";
import { customUseReducer } from "../utils/customHooks";
import { DepartmentsServices } from "../utils/services/departments";
import Theme from "../utils/theme";
import Icon from "react-native-vector-icons/Feather";
import { editProfileStart } from "../redux/actions";
import Btn from "../components/Btn";

const inputTheme = {
  placeholder: Theme.COLORS.SECONDARY,
  text: "black",
  primary: Theme.COLORS.PRIMARY,
};

const initialState = {
  departmentId: null,
  departments: [],
};

const Profile = ({ navigation }) => {
  const {
    auth: {
      dataUser: { firstName, lastName, phone, departmentId },
    },
    settings: { loader },
  } = useSelector((store) => store);

  // console.log(departmentId, "departmentId");

  const getInputsForm = () => {
    return [
      {
        label: "Nombre",
        name: "firstName",
        defaultValue: firstName,
        icon: "user",
        rules: { required: { value: true, message: "Debe escribir su nombre" } },
      },
      {
        label: "Apellido",
        name: "lastName",
        defaultValue: lastName,
        icon: "user",
        rules: { required: { value: true, message: "Debe escribir su apellido" } },
      },
      {
        label: "Teléfono",
        name: "phone",
        defaultValue: phone.toString(),
        icon: "phone",
        rules: {
          minLength: { value: 8, message: "Debe tener al menos 8 caracteres" },
          required: { value: true, message: "Debes escribir un número telefonico" },
          pattern: {
            value: /^[0-9]{0,}$/,
            message: "El telefono debe ser un numero",
          },
        },
      },
    ];
  };

  const { control, handleSubmit, errors } = useForm();
  const inputsForm = useMemo(() => getInputsForm({ control }), []);
  const dispatch = useDispatch();
  const [state, dispatchComponent] = customUseReducer(initialState);

  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    const departmentServices = new DepartmentsServices();
    const requestDepartments = await departmentServices.getDepartments();
    await dispatchComponent({
      departments: requestDepartments,
    });
  };

  const onSubmit = (data) => {
    dispatch(editProfileStart({ ...data, departmentId: state.departmentId ?? departmentId, navigation }));
  };

  return (
    <View style={{ flex: 1, justifyContent: "center", backgroundColor: "white" }}>
      <Header title="Mi cuenta" noMenu color="#3E5481" navigation={navigation} />
      <ScrollView style={{ padding: 15 }}>
        <Text style={{ color: Theme.COLORS.SECONDARY }}>Seleccione su ciudad</Text>
        <DropDownPicker
          items={state.departments.map((item) => ({
            value: item.id,
            label: item.department,
            icon: () => <Icon name="flag" size={18} color="white" />,
          }))}
          defaultValue={state.departments.length > 0 && departmentId}
          containerStyle={{ height: 50 }}
          style={{ backgroundColor: Theme.COLORS.PRIMARY, width: "100%" }}
          itemStyle={{
            justifyContent: "flex-start",
          }}
          placeholder={"Seleccione"}
          labelStyle={{ color: Theme.COLORS.WHITE }}
          dropDownStyle={{
            backgroundColor: Theme.COLORS.PRIMARY,
            width: "100%",
          }}
          onChangeItem={(item) =>
            dispatchComponent({
              departmentId: item.value,
            })
          }
        />
        {inputsForm.map((input, index) => (
          <ControllerTextInput
            key={index}
            {...input}
            errors={errors}
            control={control}
            render={({ onChange, onBlur, value }) => (
              <TextInput
                {...input}
                left={<TextInput.Icon name={() => <Icon name={input.icon} color={Theme.COLORS.SECONDARY} />} style={styles.icon} />}
                style={styles.input}
                onBlur={onBlur}
                onChangeText={(value) => onChange(value)}
                value={value}
                error={errors[input.name]}
                mode={"outlined"}
                theme={{ colors: inputTheme }}
              />
            )}
          />
        ))}

        <View style={{ justifyContent: "flex-end", marginTop: 30 }}>
          <Btn loader={loader} title={"Guardar"} color={Theme.COLORS.PRIMARY} onPress={handleSubmit(onSubmit)} />
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    marginTop: 10,
    height: 50,
  },
  submitButton: {
    height: 50,
    borderRadius: 25,
    justifyContent: "center",
    backgroundColor: Theme.COLORS.PRIMARY,
  },
  icon: {
    color: "red",
    marginTop: 15,
  },
});

export default Profile;
