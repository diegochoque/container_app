import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useSelector } from "react-redux";
import Theme from "../utils/theme";
import DropDownPicker from "react-native-dropdown-picker";
import { ShippingCompaniesServices } from "../utils/services/shippingCompanies";
import { SubTitle } from "../components";
import moment from "moment";

export default function PortSelection3({ state, dispatchComponent, presale }) {
  const {
    port: { shippingCompanies },
  } = useSelector((store) => store);

  const getDepartureDate = async (item) => {
    dispatchComponent({
      shippingCompany: { id: item.value, name: item.label },
      loading: true,
    });
    const shippingServices = new ShippingCompaniesServices();
    shippingServices.setFilterWhereDefault("shippingCompanyId", item.value);
    const requestDepartureDates = await shippingServices.getDepartureDates();
    const dates = requestDepartureDates
      .map((item) => item.rangeDate)
      .reduce((el, acc) => el.concat(acc))
      .filter((item) => moment(item).isAfter());
    const convertDates = dates.map((item) => moment(item).format("DD/MM/YYYY"));

    dispatchComponent({
      departureDate: convertDates,
      loading: false,
    });
  };

  const setDepartureDate = (item) => {
    dispatchComponent({
      destinationDate: item.label,
    });
  };

  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 20,
      }}
    >
      <SubTitle subTitle={presale ? "Elija la fecha de salida" : "Elija la naviera de su preferencia"} />
      <View style={{ flexDirection: "row", marginTop: 20, flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.textTitle}>Naviera</Text>
          {presale ? (
            <Text style={[styles.textTitle, { textAlign: "center" }]}>{state?.shippingCompany?.name}</Text>
          ) : (
            <DropDownPicker
              items={shippingCompanies.map((item) => ({
                value: item.id,
                label: item.name,
              }))}
              //defaultValue={state?.shippingCompany}
              containerStyle={{ height: 40 }}
              style={{ backgroundColor: Theme.COLORS.PRIMARY, width: "100%" }}
              itemStyle={{
                justifyContent: "flex-start",
              }}
              placeholder={"Seleccione"}
              labelStyle={{ color: Theme.COLORS.WHITE }}
              dropDownStyle={{
                backgroundColor: Theme.COLORS.PRIMARY,
                width: "100%",
              }}
              onChangeItem={getDepartureDate}
            />
          )}
        </View>
      </View>
      <View style={{ flexDirection: "row", marginTop: 20, flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.textTitle}>Fecha de salida</Text>
          <DropDownPicker
            items={state.departureDate.map((item, key) => ({
              value: key,
              label: item,
            }))}
            // defaultValue={state?.destinationDate}
            containerStyle={{ height: 40 }}
            style={{ backgroundColor: Theme.COLORS.PRIMARY, width: "100%" }}
            itemStyle={{
              justifyContent: "flex-start",
            }}
            placeholder={"Seleccione"}
            labelStyle={{ color: Theme.COLORS.WHITE }}
            dropDownStyle={{
              backgroundColor: Theme.COLORS.PRIMARY,
              width: "100%",
            }}
            onChangeItem={setDepartureDate}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    alignSelf: "center",
  },
  viewText: {
    marginTop: "14%",
    marginEnd: 12,
  },
  stext: {
    color: "#3E5481",
    fontSize: 21,
  },
  sres: {
    color: "#1FCC79",
    fontSize: 21,
  },
  sres1: {
    color: "#EB5757",
    fontSize: 21,
  },
  textTitle: {
    color: "#3E5481",
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
  contSelect: {
    marginEnd: 70,
    marginTop: 20,
  },
  contSelect2: {
    marginEnd: 70,
    marginTop: 60,
  },
  viewContainer: {
    /* backgroundColor:'blue', */
  },
  btnNext: {
    marginTop: "30%",
  },
  tipesContainers: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#1FCC79",
    marginTop: 12,
  },
});
