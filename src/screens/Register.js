import React, { useEffect, useMemo } from "react";
import { useForm } from "react-hook-form";
import { StyleSheet, Text, View } from "react-native";
import DropDownPicker from "react-native-dropdown-picker";
import { ScrollView } from "react-native-gesture-handler";
import { TextInput, Button } from "react-native-paper";
import { useDispatch, useSelector } from "react-redux";
import { SubTitle, Title } from "../components";
import ControllerTextInput from "../components/ControllerTextInput";
import { registerStart } from "../redux/actions";
import { customUseReducer } from "../utils/customHooks";
import { DepartmentsServices } from "../utils/services/departments";
import Theme from "../utils/theme";
import Icon from "react-native-vector-icons/Feather";
import Btn from "../components/Btn";

const getInputsForm = ({ control }) => {
  return [
    {
      label: "Nombre",
      name: "firstName",
      icon: "user",
      defaultValue: "",
      rules: { required: { value: true, message: "Debe escribir su nombre" } },
    },
    {
      label: "Apellido",
      name: "lastName",
      icon: "user",
      defaultValue: "",
      rules: { required: { value: true, message: "Debe escribir su apellido" } },
    },
    {
      label: "Correo electronico",
      name: "email",
      icon: "mail",
      defaultValue: "",
      rules: {
        required: { value: true, message: "Debes escribir un correo electronico" },
        pattern: {
          value: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
          message: "Debe tener un formato de email valido",
        },
      },
    },
    {
      label: "Teléfono",
      name: "phone",
      icon: "phone",
      defaultValue: "",
      rules: {
        minLength: { value: 8, message: "Debe tener al menos 8 caracteres" },
        required: { value: true, message: "Debes escribir un número telefonico" },
        pattern: {
          value: /^[0-9]{0,}$/,
          message: "El telefono debe ser un numero",
        },
      },
    },
    {
      label: "Contraseña",
      name: "password",
      icon: "lock",
      secureTextEntry: true,
      defaultValue: "",
      rules: {
        minLength: { value: 8, message: "Debe tener al menos 8 caracteres" },
        required: { value: true, message: "Debe escribir una contraseña" },
      },
    },
    {
      label: "Repita la contraseña",
      name: "repeatPassword",
      icon: "lock",
      secureTextEntry: true,
      defaultValue: "",
      validateMessageError: "Las contraseñas deben ser iguales",
      rules: {
        required: { value: true, message: "Debe escribir una contraseña" },
        validate: () => control.getValues().password === control.getValues().repeatPassword,
      },
    },
  ];
};

const inputTheme = {
  placeholder: Theme.COLORS.SECONDARY,
  text: "black",
  primary: Theme.COLORS.PRIMARY,
};

const initialState = {
  departmentId: null,
  departments: [],
};

const Register = ({ navigation }) => {
  const { control, handleSubmit, errors } = useForm();
  const inputsForm = useMemo(() => getInputsForm({ control }), []);
  const dispatch = useDispatch();
  const [state, dispatchComponent] = customUseReducer(initialState);
  const { loader } = useSelector(({ settings }) => settings);

  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    const departmentServices = new DepartmentsServices();
    const requestDepartments = await departmentServices.getDepartments();
    dispatchComponent({
      departments: requestDepartments,
    });
  };

  const onSubmit = (data) => {
    dispatch(registerStart({ ...data, departmentId: state.departmentId ?? state.departments[0]?.id, navigation }));
  };

  return (
    <View style={{ flex: 1, justifyContent: "center" }}>
      <ScrollView style={{ padding: 15 }}>
        <Title title="Bienvenido!" color={Theme.COLORS.TITLE} />
        <SubTitle subTitle="Por favor, ingrese sus datos aqui" />
        <View style={{ flex: 1, marginTop: 30 }}>
          <Text style={{ marginVertical: 10, color: Theme.COLORS.SECONDARY }}>Seleccione su ciudad</Text>
          <DropDownPicker
            items={state.departments.map((item) => ({
              value: item.id,
              label: item.department,
              icon: () => <Icon name="flag" size={18} color="white" />,
            }))}
            defaultValue={state.departments[0]?.id}
            containerStyle={{ height: 50 }}
            style={{ backgroundColor: Theme.COLORS.PRIMARY, width: "100%" }}
            itemStyle={{
              justifyContent: "flex-start",
            }}
            placeholder={"Seleccione"}
            labelStyle={{ color: Theme.COLORS.WHITE }}
            dropDownStyle={{
              backgroundColor: Theme.COLORS.PRIMARY,
              width: "100%",
            }}
            onChangeItem={(item) =>
              dispatchComponent({
                departmentId: item.value,
              })
            }
          />
          {inputsForm.map((input, index) => (
            <ControllerTextInput
              key={index}
              {...input}
              errors={errors}
              control={control}
              render={({ onChange, onBlur, value }) => (
                <TextInput
                  {...input}
                  left={<TextInput.Icon name={() => <Icon name={input.icon} color={Theme.COLORS.SECONDARY} />} style={styles.icon} />}
                  style={styles.input}
                  onBlur={onBlur}
                  onChangeText={(value) => onChange(value)}
                  value={value}
                  error={errors[input.name]}
                  mode={"outlined"}
                  theme={{ colors: inputTheme }}
                />
              )}
            />
          ))}

          <View style={{ justifyContent: "flex-end", marginTop: 30 }}>
            <Btn loader={loader} title={"Registrarme"} color={Theme.COLORS.PRIMARY} onPress={handleSubmit(onSubmit)} />
            {/* <Button style={styles.submitButton} labelStyle={{ color: "white" }} mode="contained" onPress={handleSubmit(onSubmit)}>
              Registrarme
            </Button> */}
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  input: {
    marginTop: 10,
    height: 50,
  },
  submitButton: {
    height: 50,
    borderRadius: 25,
    justifyContent: "center",
    backgroundColor: Theme.COLORS.PRIMARY,
  },
  icon: {
    color: "red",
    marginTop: 15,
  },
});

export default Register;
