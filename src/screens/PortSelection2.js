import React, { useEffect } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useSelector } from "react-redux";
import Theme from "../utils/theme";
import DropDownPicker from "react-native-dropdown-picker";
import { HarborsServices } from "../utils/services/harbors";
import { customUseReducer } from "../utils/customHooks";
import { SubTitle, Title } from "../components";
import theme from "../utils/theme";

const initialState = {
  containerRequest: [],
};

export default function PortSelection2({ state, dispatchComponent, presale }) {
  const [request, setRequest] = customUseReducer(initialState);

  useEffect(() => {
    initialRequest();
  }, []);

  const initialRequest = async () => {
    const containerService = new HarborsServices();
    containerService.setFilterWhereDefault("shippingCompanyId", state.shippingCompany.id);
    const requestContainers = await containerService.getContainers();

    let hash = {};
    let result = requestContainers
      .map((item, key) => item.companyContainer)
      .filter((element) => (hash[element.id] ? false : (hash[element.id] = true)));

    setRequest({
      containerRequest: result,
    });
  };

  const setContainer = (item) => {
    console.log(item);
    dispatchComponent({
      container: { id: item.value, name: item.label },
    });
  };

  return (
    <View
      style={{
        flex: 1,
        marginHorizontal: 20,
      }}
    >
      <SubTitle subTitle={"Elija las características que necesita del container y sus fechas"} />
      <View style={{ flexDirection: "row", marginTop: 20, flex: 1 }}>
        <View style={{ flex: 1 }}>
          <Text style={styles.textTitle}>Tamaño del contenedor</Text>
          {presale ? (
            <Title title={`Contenedor de la oferta: ${state?.container?.name}`} color={theme.COLORS.TITLE} />
          ) : (
            <DropDownPicker
              items={request.containerRequest.map((item) => ({
                value: item.id,
                label: item.dimension,
              }))}
              containerStyle={{ height: 40 }}
              style={{ backgroundColor: Theme.COLORS.PRIMARY, width: "100%" }}
              itemStyle={{
                justifyContent: "flex-start",
              }}
              placeholder={"Seleccione"}
              labelStyle={{ color: Theme.COLORS.WHITE }}
              dropDownStyle={{
                backgroundColor: Theme.COLORS.PRIMARY,
                width: "100%",
              }}
              onChangeItem={setContainer}
            />
          )}
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
    alignSelf: "center",
  },
  viewText: {
    marginTop: "14%",
    marginEnd: 12,
  },
  stext: {
    color: "#3E5481",
    fontSize: 21,
  },
  sres: {
    color: "#1FCC79",
    fontSize: 21,
  },
  sres1: {
    color: "#EB5757",
    fontSize: 21,
  },
  textTitle: {
    color: "#3E5481",
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
  contSelect: {
    marginEnd: 70,
    marginTop: 20,
  },
  contSelect2: {
    marginEnd: 70,
    marginTop: 60,
  },
  viewContainer: {
    /* backgroundColor:'blue', */
  },
  btnNext: {
    marginTop: "30%",
  },
  tipesContainers: {
    fontWeight: "bold",
    fontSize: 20,
    color: "#1FCC79",
    marginTop: 12,
  },
});
